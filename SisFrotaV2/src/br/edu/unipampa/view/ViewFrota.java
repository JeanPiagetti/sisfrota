/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.view;

import br.edu.unipampa.controller.ControllerVeiculo;
import br.edu.unipampa.model.Veiculo;
import br.edu.unipampa.persistencia.DAOVeiculo;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Huillian Serpa
 */
public class ViewFrota extends javax.swing.JFrame {

    ArrayList<Veiculo> listaVeiculo;
    String modo;
    ControllerVeiculo ctrlVeiculo = new ControllerVeiculo();

    private void carregarTabelaVeiculo() {

        DefaultTableModel modelo = new DefaultTableModel(new Object[]{"Código", "Chassis", "Placa", "Marca", "Modelo", "Cor", "Ano Fabricação", "Ano Modelo", "Cidade", "Km Rodados", "Situação", "Combustivel", "Renavan", "Nº Motor", "Tipo", "Capacidade", "Nº Assentos", "Motivo"}, 0);
        ControllerVeiculo control = new ControllerVeiculo();
        listaVeiculo = control.preencheTabela();
        for (int i = 0; i < listaVeiculo.size(); i++) {
            String situacao = "";
            if (listaVeiculo.get(i).isAtivo()) {
                situacao = "Ativo";
            } else {
                situacao = "Inativo";
            }
            Object linha[] = new Object[]{listaVeiculo.get(i).getCodVeiculo(), listaVeiculo.get(i).getChassis(), listaVeiculo.get(i).getPlaca(), listaVeiculo.get(i).getMarca(), listaVeiculo.get(i).getModelo(), listaVeiculo.get(i).getCor(), listaVeiculo.get(i).getAnoFab(), listaVeiculo.get(i).getAnoModel(), listaVeiculo.get(i).getCidade(), listaVeiculo.get(i).getQuilometragem(), situacao, listaVeiculo.get(i).getCombustivel(), listaVeiculo.get(i).getRenavan(), listaVeiculo.get(i).getNumMotor(), listaVeiculo.get(i).getTipoVeiculo(), listaVeiculo.get(i).getCapacidade(), listaVeiculo.get(i).getNumAssentos(), listaVeiculo.get(i).getMotivo()};
            modelo.addRow(linha);
            // System.out.println(linha[0] + " " + linha[1] + " " + linha[2] + " " + linha[3] + linha[2] + " " + linha[4] + " " + linha[5] + " " + linha[6] + " " + linha[7] + " " + linha[8] + " " + linha[9] + " " + linha[10] + " " + linha[11] + " " + linha[12] + " " + linha[13] + " " + linha[14] + " " + linha[15] + " " + linha[15] + " " + linha[16] + " " + linha[17]);

            tbl_veiculos.setModel(modelo);
            tbl_veiculos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        }
    }

    private void carregarTabelaVeiculosInativos() {

        DefaultTableModel modelo = new DefaultTableModel(new Object[]{"Código", "Chassis", "Placa", "Marca", "Modelo", "Cor", "Ano Fabricação", "Ano Modelo", "Cidade", "Km Rodados", "Situação", "Combustivel", "Renavan", "Nº Motor", "Tipo", "Capacidade", "Nº Assentos", "Motivo"}, 0);
        ControllerVeiculo control = new ControllerVeiculo();
        listaVeiculo = control.lerInativos();
        for (int i = 0; i < listaVeiculo.size(); i++) {
            String situacao = "";
            if (listaVeiculo.get(i).isAtivo()) {
                situacao = "Ativo";
            } else {
                situacao = "Inativo";
            }
            Object linha[] = new Object[]{listaVeiculo.get(i).getCodVeiculo(), listaVeiculo.get(i).getChassis(), listaVeiculo.get(i).getPlaca(), listaVeiculo.get(i).getMarca(), listaVeiculo.get(i).getModelo(), listaVeiculo.get(i).getCor(), listaVeiculo.get(i).getAnoFab(), listaVeiculo.get(i).getAnoModel(), listaVeiculo.get(i).getCidade(), listaVeiculo.get(i).getQuilometragem(), situacao, listaVeiculo.get(i).getCombustivel(), listaVeiculo.get(i).getRenavan(), listaVeiculo.get(i).getNumMotor(), listaVeiculo.get(i).getTipoVeiculo(), listaVeiculo.get(i).getCapacidade(), listaVeiculo.get(i).getNumAssentos(), listaVeiculo.get(i).getMotivo()};
            modelo.addRow(linha);
            // System.out.println(linha[0] + " " + linha[1] + " " + linha[2] + " " + linha[3] + linha[2] + " " + linha[4] + " " + linha[5] + " " + linha[6] + " " + linha[7] + " " + linha[8] + " " + linha[9] + " " + linha[10] + " " + linha[11] + " " + linha[12] + " " + linha[13] + " " + linha[14] + " " + linha[15] + " " + linha[15] + " " + linha[16] + " " + linha[17]);

            tbl_veiculos.setModel(modelo);
            tbl_veiculos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        }
    }

    private void carregarTabelaVeiculosAtivos() {

        DefaultTableModel modelo = new DefaultTableModel(new Object[]{"Código", "Chassis", "Placa", "Marca", "Modelo", "Cor", "Ano Fabricação", "Ano Modelo", "Cidade", "Km Rodados", "Situação", "Combustivel", "Renavan", "Nº Motor", "Tipo", "Capacidade", "Nº Assentos", "Motivo"}, 0);
        ControllerVeiculo control = new ControllerVeiculo();
        listaVeiculo = control.lerAtivos();
        for (int i = 0; i < listaVeiculo.size(); i++) {
            String situacao = "";
            if (listaVeiculo.get(i).isAtivo()) {
                situacao = "Ativo";
            } else {
                situacao = "Inativo";
            }
            Object linha[] = new Object[]{listaVeiculo.get(i).getCodVeiculo(), listaVeiculo.get(i).getChassis(), listaVeiculo.get(i).getPlaca(), listaVeiculo.get(i).getMarca(), listaVeiculo.get(i).getModelo(), listaVeiculo.get(i).getCor(), listaVeiculo.get(i).getAnoFab(), listaVeiculo.get(i).getAnoModel(), listaVeiculo.get(i).getCidade(), listaVeiculo.get(i).getQuilometragem(), situacao, listaVeiculo.get(i).getCombustivel(), listaVeiculo.get(i).getRenavan(), listaVeiculo.get(i).getNumMotor(), listaVeiculo.get(i).getTipoVeiculo(), listaVeiculo.get(i).getCapacidade(), listaVeiculo.get(i).getNumAssentos(), listaVeiculo.get(i).getMotivo()};
            modelo.addRow(linha);
            // System.out.println(linha[0] + " " + linha[1] + " " + linha[2] + " " + linha[3] + linha[2] + " " + linha[4] + " " + linha[5] + " " + linha[6] + " " + linha[7] + " " + linha[8] + " " + linha[9] + " " + linha[10] + " " + linha[11] + " " + linha[12] + " " + linha[13] + " " + linha[14] + " " + linha[15] + " " + linha[15] + " " + linha[16] + " " + linha[17]);

            tbl_veiculos.setModel(modelo);
            tbl_veiculos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        }
    }

    public void manipulaInterface() {
        switch (modo) {
            case "Navegar":
                btn_veiculo_salvar.setEnabled(false);
                btn_veiculo_cancelar.setEnabled(false);
                btn_veiculo_editar.setEnabled(true);
                btn_veiculo_editar.setEnabled(true);
                btn_veiculo_novo.setEnabled(true);
                //  tf_tarifa_valor.setEnabled(false);
                //comboBox_veiculo_tipoTarifa.setEnabled(false);
                //chb_emVigor.setEnabled(false);
                break;
            case "Novo":
                btn_veiculo_salvar.setEnabled(true);
                btn_veiculo_cancelar.setEnabled(true);
                btn_veiculo_editar.setEnabled(false);
                btn_veiculo_excluir.setEnabled(false);
                btn_veiculo_novo.setEnabled(false);
                //tf_tarifa_valor.setEnabled(true);
                //comboBox_veiculo_tipoTarifa.setEnabled(true);
                //chb_emVigor.setEnabled(true);
                break;
            case "Editar":
                btn_veiculo_salvar.setEnabled(true);
                btn_veiculo_cancelar.setEnabled(true);
                btn_veiculo_editar.setEnabled(true);
                btn_veiculo_excluir.setEnabled(false);
                btn_veiculo_novo.setEnabled(true);
                //tf_tarifa_valor.setEnabled(true);
                //comboBox_veiculo_tipoTarifa.setEnabled(true);
                //chb_emVigor.setEnabled(true);
                break;
            case "Excluir":
                btn_veiculo_salvar.setEnabled(false);
                btn_veiculo_cancelar.setEnabled(false);
                btn_veiculo_editar.setEnabled(false);
                btn_veiculo_excluir.setEnabled(false);
                btn_veiculo_novo.setEnabled(true);
                //tf_tarifa_valor.setEnabled(false);
                //comboBox_veiculo_tipoTarifa.setEnabled(false);
                //chb_emVigor.setEnabled(false);
                break;
            case "Selecao":
                btn_veiculo_salvar.setEnabled(false);
                btn_veiculo_cancelar.setEnabled(false);
                btn_veiculo_editar.setEnabled(true);
                btn_veiculo_excluir.setEnabled(true);
                btn_veiculo_novo.setEnabled(true);
                //tf_tarifa_valor.setEnabled(false);
                //comboBox_veiculo_tipoTarifa.setEnabled(false);
                //chb_emVigor.setEnabled(false);
                break;
            default:
                System.out.println("Modo Inválido");

        }
    }

    /**
     * Creates new form ViewFrota
     */
    public ViewFrota() {
        initComponents();
        setLocationRelativeTo(null);
        modo = "Navegar";
        carregarTabelaVeiculo();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        tf_veiculo_placa = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        tf_veiculo_renavan = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        tf_veiculo_anoFab = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        tf_veiculo_anoModelo = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        tf_veiculo_marca = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        tf_veiculo_modelo = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        tf_veiculo_cor = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        tf_veiculo_chassis = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        tf_veiculo_numMotor = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        tf_veiculo_cidade = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        tf_veiculo_numAssentos = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        tf_veiculo_capacidade = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        tf_veiculo_tipoVeiculo = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        tf_veiculo_quilometragem = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        tf_veiculo_combustivel = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        cb_veiculo_situacao = new javax.swing.JCheckBox();
        jLabel17 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tf_veiculo_motivo = new javax.swing.JTextArea();
        btn_veiculo_novo = new javax.swing.JButton();
        btn_veiculo_salvar = new javax.swing.JButton();
        btn_veiculo_cancelar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_veiculos = new javax.swing.JTable();
        btn_veiculo_todos = new javax.swing.JButton();
        btn_veiculo_ativos = new javax.swing.JButton();
        btn_veiculo_inativos = new javax.swing.JButton();
        btn_veiculo_editar = new javax.swing.JButton();
        btn_veiculo_excluir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Novo Veiculo"));

        jLabel1.setText("Placa:");

        jLabel2.setText("Renavan:");

        jLabel3.setText("Ano Fabricação:");

        jLabel4.setText("Ano Modelo:");

        jLabel5.setText("Marca:");

        jLabel6.setText("Modelo:");

        jLabel7.setText("Cor:");

        jLabel8.setText("Chassis:");

        jLabel9.setText("Nº Motor:");

        jLabel10.setText("Cidade:");

        tf_veiculo_cidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_veiculo_cidadeActionPerformed(evt);
            }
        });

        jLabel11.setText("Nº Assentos:");

        jLabel12.setText("Capacidade:");

        jLabel13.setText("Tipo Veículo:");

        jLabel14.setText("Km Rodados:");

        jLabel15.setText("Combustível:");

        jLabel16.setText("Situação:");

        jLabel17.setText("Motivo:");

        tf_veiculo_motivo.setColumns(20);
        tf_veiculo_motivo.setRows(5);
        jScrollPane1.setViewportView(tf_veiculo_motivo);

        btn_veiculo_novo.setText("Novo");
        btn_veiculo_novo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_veiculo_novoActionPerformed(evt);
            }
        });

        btn_veiculo_salvar.setText("Salvar");
        btn_veiculo_salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_veiculo_salvarActionPerformed(evt);
            }
        });

        btn_veiculo_cancelar.setText("Cancelar");
        btn_veiculo_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_veiculo_cancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6)
                    .addComponent(jLabel5)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(tf_veiculo_cor, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                            .addComponent(tf_veiculo_modelo, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tf_veiculo_marca, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(tf_veiculo_anoModelo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE)
                            .addComponent(tf_veiculo_anoFab, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(tf_veiculo_renavan, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel9))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(tf_veiculo_placa, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(170, 170, 170)
                        .addComponent(jLabel8)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tf_veiculo_chassis)
                            .addComponent(tf_veiculo_numMotor, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                            .addComponent(tf_veiculo_cidade, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tf_veiculo_numAssentos, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(86, 86, 86)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel15)
                            .addComponent(jLabel16)
                            .addComponent(jLabel17)))
                    .addComponent(tf_veiculo_tipoVeiculo, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_veiculo_quilometragem, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_veiculo_capacidade, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tf_veiculo_combustivel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_veiculo_situacao)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_veiculo_novo)
                    .addComponent(btn_veiculo_salvar)
                    .addComponent(btn_veiculo_cancelar))
                .addGap(21, 21, 21))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(tf_veiculo_placa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(tf_veiculo_chassis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(tf_veiculo_combustivel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_veiculo_novo))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(tf_veiculo_renavan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9)
                            .addComponent(tf_veiculo_numMotor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16)
                            .addComponent(cb_veiculo_situacao)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(btn_veiculo_salvar)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel3)
                                    .addComponent(tf_veiculo_anoFab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel10)
                                    .addComponent(tf_veiculo_cidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel17))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(tf_veiculo_anoModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel11)
                                    .addComponent(tf_veiculo_numAssentos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5)
                                    .addComponent(tf_veiculo_marca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel12)
                                    .addComponent(tf_veiculo_capacidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel6)
                                    .addComponent(tf_veiculo_modelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel13)
                                    .addComponent(tf_veiculo_tipoVeiculo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel7)
                                    .addComponent(tf_veiculo_cor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel14)
                                    .addComponent(tf_veiculo_quilometragem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jScrollPane1))
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(btn_veiculo_cancelar)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Gerenciar Frota "));

        tbl_veiculos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Código", "Placa", "Renavan", "Ano Fabri", "Ano Model", "Marca", "Modelo", "Cor", "Chassis", "Nº Motor", "Cidade", "Nº Assentos", "Capacidade", "Tipo", "Km Rodados", "Combustível", "Situação", "Motivo"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_veiculos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_veiculosMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tbl_veiculos);

        btn_veiculo_todos.setText("Todos");
        btn_veiculo_todos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_veiculo_todosActionPerformed(evt);
            }
        });

        btn_veiculo_ativos.setText("Ativos");
        btn_veiculo_ativos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_veiculo_ativosActionPerformed(evt);
            }
        });

        btn_veiculo_inativos.setText("Inativos");
        btn_veiculo_inativos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_veiculo_inativosActionPerformed(evt);
            }
        });

        btn_veiculo_editar.setText("Editar");
        btn_veiculo_editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_veiculo_editarActionPerformed(evt);
            }
        });

        btn_veiculo_excluir.setText("Excluir");
        btn_veiculo_excluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_veiculo_excluirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 971, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 80, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_veiculo_ativos)
                    .addComponent(btn_veiculo_todos)
                    .addComponent(btn_veiculo_inativos)
                    .addComponent(btn_veiculo_editar)
                    .addComponent(btn_veiculo_excluir))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(btn_veiculo_todos)
                .addGap(18, 18, 18)
                .addComponent(btn_veiculo_ativos)
                .addGap(18, 18, 18)
                .addComponent(btn_veiculo_inativos)
                .addGap(18, 18, 18)
                .addComponent(btn_veiculo_editar)
                .addGap(18, 18, 18)
                .addComponent(btn_veiculo_excluir)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jScrollPane2.setViewportView(jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1136, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 546, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_veiculo_salvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_veiculo_salvarActionPerformed
        if (modo.equals("Novo")) {
            boolean situacao;
            if (cb_veiculo_situacao.isSelected()) {
                situacao = true;
            } else {
                situacao = false;
            }
            String chassis = tf_veiculo_chassis.getText();
            String placa = tf_veiculo_placa.getText();
            String marca = tf_veiculo_marca.getText();
            String modelo = tf_veiculo_modelo.getText();
            String cor = tf_veiculo_cor.getText();
            String dataFab = tf_veiculo_anoFab.getText();
            java.util.Date date1 = null;
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            try {
                date1 = sdf.parse(dataFab);
            } catch (ParseException ex) {
                //Logger.getLogger(ViewVeiculo.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("O ERRO FOI:" + ex.getMessage());
            }
            String dataModelo = tf_veiculo_anoModelo.getText();
            java.util.Date date2 = null;
            try {
                date2 = sdf.parse(dataModelo);
            } catch (ParseException ex) {
                //Logger.getLogger(ViewVeiculo.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("O ERRO FOI:" + ex.getMessage());
            }

            String cidade = tf_veiculo_cidade.getText();
            float quilometragem = Float.parseFloat(tf_veiculo_quilometragem.getText());
            float combustivel = Float.parseFloat(tf_veiculo_combustivel.getText());
            String renavan = tf_veiculo_renavan.getText();
            String numMotor = tf_veiculo_numMotor.getText();
            String tipoVeiculo = tf_veiculo_tipoVeiculo.getText();
            int capacidade = Integer.parseInt(tf_veiculo_capacidade.getText());
            int numAssentos = Integer.parseInt(tf_veiculo_numAssentos.getText());
            String motivo = tf_veiculo_motivo.getText();
            Veiculo v = new Veiculo(chassis, placa, marca, modelo, cor, date1, date2,
                    cidade, quilometragem, situacao, combustivel, renavan, numMotor, tipoVeiculo, capacidade, numAssentos, motivo);

            DAOVeiculo dao = new DAOVeiculo();
            dao.inserir(v);
            carregarTabelaVeiculo();
        } else if (modo.equals("Editar")) {
            int index = tbl_veiculos.getSelectedRow();
            Veiculo veiculoAux = listaVeiculo.get(index);
            
            
            
            boolean situacao = cb_veiculo_situacao.isSelected();
            veiculoAux.setSituacao(situacao);
            veiculoAux.setChassis(tf_veiculo_chassis.getText());
            veiculoAux.setPlaca(tf_veiculo_placa.getText());
            veiculoAux.setMarca(tf_veiculo_marca.getText());
            veiculoAux.setModelo(tf_veiculo_modelo.getText());
            veiculoAux.setCor(tf_veiculo_cor.getText());
            String dataFab = tf_veiculo_anoFab.getText();
            java.util.Date date1 = null;
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            try {
                date1 = sdf.parse(dataFab);
                veiculoAux.setAnoFab(date1);
            } catch (ParseException ex) {
                //Logger.getLogger(ViewVeiculo.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("O ERRO FOI:" + ex.getMessage());
            }
            String dataModelo = tf_veiculo_anoModelo.getText();
            java.util.Date date2 = null;
            try {
                date2 = sdf.parse(dataModelo);
                veiculoAux.setAnoModel(date2);
            } catch (ParseException ex) {
                //Logger.getLogger(ViewVeiculo.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("O ERRO FOI:" + ex.getMessage());
            }
            veiculoAux.setCidade(tf_veiculo_cidade.getText());
            float quilometragem = Float.parseFloat(tf_veiculo_quilometragem.getText());
            veiculoAux.setQuilometragem(quilometragem);
            float combustivel = Float.parseFloat(tf_veiculo_combustivel.getText());
            veiculoAux.setCombustivel(combustivel);
            veiculoAux.setRenavan(tf_veiculo_renavan.getText());
            veiculoAux.setNumMotor(tf_veiculo_numMotor.getText());
            veiculoAux.setTipoVeiculo(tf_veiculo_tipoVeiculo.getText());
            int capacidade = Integer.parseInt(tf_veiculo_capacidade.getText());
            veiculoAux.setCapacidade(capacidade);
            int numAssentos = Integer.parseInt(tf_veiculo_numAssentos.getText());
            veiculoAux.setNumAssentos(numAssentos);
            veiculoAux.setMotivo(tf_veiculo_motivo.getText());
            
            
            listaVeiculo.set(index, veiculoAux);

            DAOVeiculo dao = new DAOVeiculo();
            dao.editar(veiculoAux);
            carregarTabelaVeiculo();

        }
        manipulaInterface();
    }//GEN-LAST:event_btn_veiculo_salvarActionPerformed

    private void btn_veiculo_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_veiculo_cancelarActionPerformed
        modo = "Navegar";
        manipulaInterface();
    }//GEN-LAST:event_btn_veiculo_cancelarActionPerformed

    private void btn_veiculo_todosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_veiculo_todosActionPerformed
        carregarTabelaVeiculo();
    }//GEN-LAST:event_btn_veiculo_todosActionPerformed

    private void btn_veiculo_ativosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_veiculo_ativosActionPerformed
        carregarTabelaVeiculosAtivos();

    }//GEN-LAST:event_btn_veiculo_ativosActionPerformed

    private void btn_veiculo_novoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_veiculo_novoActionPerformed
        modo = "Novo";
        manipulaInterface();
    }//GEN-LAST:event_btn_veiculo_novoActionPerformed

    private void tbl_veiculosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_veiculosMouseClicked
        int index = tbl_veiculos.getSelectedRow();
        if (index >= 0 && index <= listaVeiculo.size()) {
            Veiculo veic = listaVeiculo.get(index);
            cb_veiculo_situacao.setSelected(veic.isAtivo());
            tf_veiculo_chassis.setText(String.valueOf(veic.getChassis()));
            tf_veiculo_placa.setText(String.valueOf(veic.getPlaca()));
            tf_veiculo_marca.setText(String.valueOf(veic.getMarca()));
            tf_veiculo_modelo.setText(String.valueOf(veic.getModelo()));
            tf_veiculo_cor.setText(String.valueOf(veic.getCor()));
            tf_veiculo_anoFab.setText(String.valueOf(veic.getAnoFab()));
            tf_veiculo_anoModelo.setText(String.valueOf(veic.getAnoModel()));
            tf_veiculo_renavan.setText(String.valueOf(veic.getRenavan()));
            tf_veiculo_numMotor.setText(veic.getNumMotor());
            tf_veiculo_cidade.setText(veic.getCidade());
            tf_veiculo_numAssentos.setText(String.valueOf(veic.getNumAssentos()));
            tf_veiculo_capacidade.setText(String.valueOf(veic.getCapacidade()));
            tf_veiculo_tipoVeiculo.setText(veic.getTipoVeiculo());
            tf_veiculo_quilometragem.setText(String.valueOf(veic.getQuilometragem()));
            tf_veiculo_combustivel.setText(String.valueOf(veic.getCombustivel()));
            tf_veiculo_motivo.setText(veic.getMotivo());
            //comboBox_veiculo_tipoTarifa.setActionCommand(tar.getTipo());
            modo = "Selecao";
            manipulaInterface();
        }
    }//GEN-LAST:event_tbl_veiculosMouseClicked

    private void tf_veiculo_cidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tf_veiculo_cidadeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tf_veiculo_cidadeActionPerformed

    private void btn_veiculo_editarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_veiculo_editarActionPerformed
        modo = "Editar";
        manipulaInterface();
    }//GEN-LAST:event_btn_veiculo_editarActionPerformed

    private void btn_veiculo_excluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_veiculo_excluirActionPerformed
        int index = tbl_veiculos.getSelectedRow();
        if (tbl_veiculos.getSelectedRow() != -1) {
            DefaultTableModel modelo = (DefaultTableModel) tbl_veiculos.getModel();
            modelo.removeRow(tbl_veiculos.getSelectedRow());
            //ControllerPagamento controller = new ControllerPagamento();
            //controller.removerDadosTabela(p);
            
        } else {
            JOptionPane.showMessageDialog(null, "Selecione um veiculo para excluir");
        }

    }//GEN-LAST:event_btn_veiculo_excluirActionPerformed

    private void btn_veiculo_inativosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_veiculo_inativosActionPerformed
        carregarTabelaVeiculosInativos();
    }//GEN-LAST:event_btn_veiculo_inativosActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ViewFrota.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ViewFrota.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ViewFrota.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ViewFrota.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ViewFrota().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_veiculo_ativos;
    private javax.swing.JButton btn_veiculo_cancelar;
    private javax.swing.JButton btn_veiculo_editar;
    private javax.swing.JButton btn_veiculo_excluir;
    private javax.swing.JButton btn_veiculo_inativos;
    private javax.swing.JButton btn_veiculo_novo;
    private javax.swing.JButton btn_veiculo_salvar;
    private javax.swing.JButton btn_veiculo_todos;
    private javax.swing.JCheckBox cb_veiculo_situacao;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable tbl_veiculos;
    private javax.swing.JTextField tf_veiculo_anoFab;
    private javax.swing.JTextField tf_veiculo_anoModelo;
    private javax.swing.JTextField tf_veiculo_capacidade;
    private javax.swing.JTextField tf_veiculo_chassis;
    private javax.swing.JTextField tf_veiculo_cidade;
    private javax.swing.JTextField tf_veiculo_combustivel;
    private javax.swing.JTextField tf_veiculo_cor;
    private javax.swing.JTextField tf_veiculo_marca;
    private javax.swing.JTextField tf_veiculo_modelo;
    private javax.swing.JTextArea tf_veiculo_motivo;
    private javax.swing.JTextField tf_veiculo_numAssentos;
    private javax.swing.JTextField tf_veiculo_numMotor;
    private javax.swing.JTextField tf_veiculo_placa;
    private javax.swing.JTextField tf_veiculo_quilometragem;
    private javax.swing.JTextField tf_veiculo_renavan;
    private javax.swing.JTextField tf_veiculo_tipoVeiculo;
    // End of variables declaration//GEN-END:variables
}
