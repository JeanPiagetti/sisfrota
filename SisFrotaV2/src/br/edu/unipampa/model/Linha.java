package br.edu.unipampa.model;

public class Linha {

    private int codigo;
    private String origem;
    private String destino;
    //private float valor;
    //private Tarifa tarifa;

    public Linha(int codigo, String origem, String destino) {
        this.codigo = codigo;
        //this.valor = valor;
        // this.tarifa = tarifa;
        this.origem = origem;
        this.destino = destino;
    }

    public Linha(String origem, String destino) {

        //this.valor = valor;
        this.origem = origem;
        this.destino = destino;
    }

    public Linha() {

    }

//    public float getValor() {
//        return valor;
//    }
//
//    public void setValor(float valor) {
//        this.valor = valor;
//    }
//    public Tarifa getTarifa() {
//        return tarifa;
//    }
//
//    public void setTarifa(Tarifa tarifa) {
//        this.tarifa = tarifa;
//    }
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

}
