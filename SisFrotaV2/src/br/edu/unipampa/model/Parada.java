package br.edu.unipampa.model;


import java.time.LocalTime;


public class Parada {

    private String local;
    private LocalTime horario;

    public Parada(String local, LocalTime horario) {
        this.local = local;
        this.horario = horario;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public LocalTime getHorario() {
        return horario;
    }

    public void setHorario(LocalTime horario) {
        this.horario = horario;
    }

}
