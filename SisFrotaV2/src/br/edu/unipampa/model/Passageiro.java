/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.model;

/**
 *
 * @author JPiagetti
 */
public class Passageiro {

    private long cpf;
    private String nomePassageiro;
    private int codPassageiro;
    private int idade;

    public Passageiro(long cpf, String nomePassageiro, int codPassageiro, int idade) {
        this.cpf = cpf;
        this.nomePassageiro = nomePassageiro;
        this.codPassageiro = codPassageiro;
        this.idade = idade;
    }

    public Passageiro() {
    }

    public long getCpf() {
        return cpf;
    }

    public void setCpf(long cpf) {
        this.cpf = cpf;
    }

    public String getNomePassageiro() {
        return nomePassageiro;
    }

    public void setNomePassageiro(String nomePassageiro) {
        this.nomePassageiro = nomePassageiro;
    }

    public int getCodPassageiro() {
        return codPassageiro;
    }

    public void setCodPassageiro(int codPassageiro) {
        this.codPassageiro = codPassageiro;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

}
