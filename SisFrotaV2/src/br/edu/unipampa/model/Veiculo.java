package br.edu.unipampa.model;

import java.util.Date;

public class Veiculo {

    private int codVeiculo;
    private String chassis;
    private String placa;
    private String marca;
    private String modelo;
    private String cor;
    private Date anoFab;
    private Date anoModel;
    private String cidade;
    private float quilometragem;
    private boolean situacao;
    private float combustivel;
    private String renavan;
    private String numMotor;
    private String tipoVeiculo;
    private int capacidade;
    private int numAssentos;
    private String motivo;
    private Linha linha;

    public Veiculo(String chassis, String placa, String marca, String modelo,
            String cor, Date anoFab, Date anoModel, String cidade, float quilometragem,
            boolean situacao, float combustivel, String renavan, String numMotor, String tipoVeiculo,
            int capacidade, int numAssentos, String motivo) {
        this.chassis = chassis;
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.cor = cor;
        this.anoFab = anoFab;
        this.anoModel = anoModel;
        this.cidade = cidade;
        this.quilometragem = quilometragem;
        this.situacao = situacao;
        this.combustivel = combustivel;
        this.renavan = renavan;
        this.numMotor = numMotor;
        this.tipoVeiculo = tipoVeiculo;
        this.capacidade = capacidade;
        this.numAssentos = numAssentos;
        this.motivo = motivo;
    }

    public Veiculo(int codVeiculo, String chassis, String placa, String marca,
            String modelo, String cor, Date anoFab, Date anoModel, String cidade,
            float quilometragem, boolean situacao, float combustivel, String renavan,
            String numMotor, String tipoVeiculo, int capacidade, int numAssentos, String motivo,
            Linha linha) {
        this.codVeiculo = codVeiculo;
        this.chassis = chassis;
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.cor = cor;
        this.anoFab = anoFab;
        this.anoModel = anoModel;
        this.cidade = cidade;
        this.quilometragem = quilometragem;
        this.situacao = situacao;
        this.combustivel = combustivel;
        this.renavan = renavan;
        this.numMotor = numMotor;
        this.tipoVeiculo = tipoVeiculo;
        this.capacidade = capacidade;
        this.numAssentos = numAssentos;
        this.motivo = motivo;
        this.linha = linha;
    }

    public Veiculo() {

    }

    public int getCodVeiculo() {
        return codVeiculo;
    }

    public void setCodVeiculo(int codVeiculo) {
        this.codVeiculo = codVeiculo;
    }

    public String getChassis() {
        return chassis;
    }

    public void setChassis(String chassis) {
        this.chassis = chassis;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public Date getAnoFab() {
        return anoFab;
    }

    public void setAnoFab(Date anoFab) {
        this.anoFab = anoFab;
    }

    public Date getAnoModel() {
        return anoModel;
    }

    public void setAnoModel(Date anoModel) {
        this.anoModel = anoModel;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public float getQuilometragem() {
        return quilometragem;
    }

    public void setQuilometragem(float quilometragem) {
        this.quilometragem = quilometragem;
    }

    public boolean isAtivo() {
        return situacao;
    }

    public void setSituacao(boolean situacao) {
        this.situacao = situacao;
    }

    public float getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(float combustivel) {
        this.combustivel = combustivel;
    }

    public String getRenavan() {
        return renavan;
    }

    public void setRenavan(String renavan) {
        this.renavan = renavan;
    }

    public String getNumMotor() {
        return numMotor;
    }

    public void setNumMotor(String numMotor) {
        this.numMotor = numMotor;
    }

    public String getTipoVeiculo() {
        return tipoVeiculo;
    }

    public void setTipoVeiculo(String tipoVeiculo) {
        this.tipoVeiculo = tipoVeiculo;
    }

    public int getNumAssentos() {
        return numAssentos;
    }

    public void setNumAssentos(int numAssentos) {
        this.numAssentos = numAssentos;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public Linha getLinha() {
        return linha;
    }

    public void setLinha(Linha linha) {
        this.linha = linha;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

}
