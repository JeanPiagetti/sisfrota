/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.model;

/**
 *
 * @author JPiagetti
 */
public class Pagamento {

    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
     */
    private int idPagamento;
    private String nomeFormaPagamento;
    private String tipoPagamento;
    private String linha;
    private boolean situacao;

    public Pagamento(int idPagamento, String nomeFormaPagamento, String tipoPagamento, String linha) {
        this.idPagamento = idPagamento;
        this.nomeFormaPagamento = nomeFormaPagamento;
        this.tipoPagamento = tipoPagamento;
        this.linha = linha;
    }

    public Pagamento(int idPagamento) {
        this.idPagamento = idPagamento;
    }

    public Pagamento() {
    }

    public Pagamento(String nomeFormaPagamento, String tipoPagamento, String linha, boolean situacao) {
        this.nomeFormaPagamento = nomeFormaPagamento;
        this.tipoPagamento = tipoPagamento;
        this.linha = linha;
        this.situacao = situacao;
    }

    public boolean isSituacao() {
        return situacao;
    }

    public void setSituacao(boolean situacao) {
        this.situacao = situacao;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public Pagamento(String nomeFormaPagamento, String tipoPagamento, String linha) {
        this.nomeFormaPagamento = nomeFormaPagamento;
        this.tipoPagamento = tipoPagamento;
        this.linha = linha;
    }

    public int getIdPagamento() {
        return idPagamento;
    }

    public void setIdPagamento(int idPagamento) {
        this.idPagamento = idPagamento;
    }

    public String getNomeFormaPagamento() {
        return nomeFormaPagamento;
    }

    public void setNomeFormaPagamento(String nomeFormaPagamento) {
        this.nomeFormaPagamento = nomeFormaPagamento;
    }

    public String getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(String tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

}
