/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.model;

/**
 *
 * @author JPiagetti
 */
public class Assento {

    private int codAssento;
    private String tipoAssento;
    private final int QUANTIDADE = 50;
    private int codVeiculo;
    private boolean situacao;
    private double taxaAssento;

    public Assento() {
    }

    public Assento(String tipoAssento, int codVeiculo, boolean situacao, double taxaAssento) {
        this.tipoAssento = tipoAssento;
        this.codVeiculo = codVeiculo;
        this.situacao = situacao;
        this.taxaAssento = taxaAssento;
    }

    public Assento(String tipoAssento) {
        this.tipoAssento = tipoAssento;
    }

    public Assento(int codAssento, String tipoAssento, int codVeiculo, boolean situacao) {
        this.codAssento = codAssento;
        this.tipoAssento = tipoAssento;
        this.codVeiculo = codVeiculo;
        this.situacao = situacao;
    }

    public Assento(String tipoAssento, int codVeiculo, boolean situacao) {
        this.tipoAssento = tipoAssento;
        this.codVeiculo = codVeiculo;
        this.situacao = situacao;
    }

    public double getTaxaAssento() {
        return taxaAssento;
    }

    public void setTaxaAssento(double taxaAssento) {
        this.taxaAssento = taxaAssento;
    }

    public boolean isSituacao() {
        return situacao;
    }

    public void setSituacao(boolean situacao) {
        this.situacao = situacao;
    }

    public int getQUANTIDADE() {
        return QUANTIDADE;
    }

    public int getCodAssento() {
        return codAssento;
    }

    public void setCodAssento(int codAssento) {
        this.codAssento = codAssento;
    }

    public String getTipoAssento() {
        return tipoAssento;
    }

    public void setTipoAssento(String tipoAssento) {
        this.tipoAssento = tipoAssento;
    }

    public int getCodVeiculo() {
        return codVeiculo;
    }

    public void setCodVeiculo(int codVeiculo) {
        this.codVeiculo = codVeiculo;
    }

}
