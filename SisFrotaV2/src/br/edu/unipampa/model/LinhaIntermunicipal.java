package br.edu.unipampa.model;

//import java.util.ArrayList;


public class LinhaIntermunicipal extends Linha {

    //private ArrayList <Trecho> trechos;

    public LinhaIntermunicipal(int codigo,  String origem, String destino) {
        super( codigo,origem, destino);
        //trechos = new ArrayList();
    }
        
    public LinhaIntermunicipal(  String origem, String destino) {
        super( origem, destino);
        //trechos = new ArrayList();
    }

    public LinhaIntermunicipal() {
        
    }
    
//    public boolean adicionarTrecho(Trecho trecho){
//        try{
//        trechos.add(trecho);
//        return true;
//        }catch(Exception e){
//            System.out.println("O erro é:"+e.getMessage());
//            return false;
//        }
//    }
    
//    public int tamanhoLista(){
//        return trechos.size();
//    }
//    
//    public String toString(){
//        String conteudo = "Codigo:"+LinhaIntermunicipal.super.getCodigo() 
//                +"\n Origem"+LinhaIntermunicipal.super.getOrigem()
//                +"\n Destino"+LinhaIntermunicipal.super.getDestino();
//        for (int i = 0; i < trechos.size(); i++) {
//            trechos.get(i);
//            conteudo = conteudo+"\n ponto do trecho de origem:"+trechos.get(i).getOrigem()+"\n ponto do trecho de destino:"+trechos.get(i).getDestino()
//                    +"\n Distância:"+trechos.get(i).getDistancia();
//        }
//        return conteudo;
//    }
//    
}
