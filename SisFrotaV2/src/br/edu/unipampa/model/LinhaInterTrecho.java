/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.model;

/**
 *
 * @author JPiagetti
 */
public class LinhaInterTrecho {

    private int codLinhaInter;
    private String descricao;
    private int codTrecho;

    public LinhaInterTrecho() {
    }

    public LinhaInterTrecho(int codLinhaInter, String descricao, int codTrecho) {
        this.codLinhaInter = codLinhaInter;
        this.descricao = descricao;
        this.codTrecho = codTrecho;
    }

    public int getCodLinhaInter() {
        return codLinhaInter;
    }

    public void setCodLinhaInter(int codLinhaInter) {
        this.codLinhaInter = codLinhaInter;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getCodTrecho() {
        return codTrecho;
    }

    public void setCodTrecho(int codTrecho) {
        this.codTrecho = codTrecho;
    }

}
