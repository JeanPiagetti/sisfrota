package br.edu.unipampa.model;

import java.util.ArrayList;

public class Trecho {

   
    private int codigo;
    private String origem;
    private String destino;
    private float distancia;
    private ArrayList<LinhaIntermunicipal> LinhasIntermunicipais;
    
    

    public Trecho( String origem, String destino, float distancia) {
        
        this.origem = origem;
        this.destino = destino;
        this.distancia = distancia;
        LinhasIntermunicipais = new ArrayList();
        
        
    }
    public Trecho(){
        
    }

     public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public float getDistancia() {
        return distancia;
    }

    public void setDistancia(float distancia) {
        this.distancia = distancia;
    }

//    public Tarifa getTarifa() {
//        return tarifa;
//    }
//
//    public void setTarifa(Tarifa tarifa) {
//        this.tarifa = tarifa;
//    }

//    public float getValor() {
//        return km * tarifa.getValor();
//    }
//    
//    public void setValor(float valor) {
//        this.valor = valor;
//    }

}
