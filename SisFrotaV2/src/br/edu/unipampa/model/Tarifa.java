/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.model;

/**
 *
 * @author JPiagetti
 */
public class Tarifa {

    private int codTarifa;
    private double valor;
    private String tipoTarifa;
    private boolean emVigor;

    public Tarifa() {
    }

    public Tarifa(int codTarifa, float valor, String tipoTarfifa, boolean emVigor) {
        this.codTarifa = codTarifa;
        this.valor = valor;
        this.tipoTarifa = tipoTarfifa;
        this.emVigor = emVigor;
    }

    public Tarifa(String tipoTarfifa) {
        this.tipoTarifa = tipoTarfifa;
    }

    public int getCodTarifa() {
        return codTarifa;
    }

    public void setCodTarifa(int codTarifa) {
        this.codTarifa = codTarifa;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public String getTipoTarifa() {
        return tipoTarifa;
    }

    public void setTipoTarifa(String tipoTarifa) {
        this.tipoTarifa = tipoTarifa;
    }

    public boolean isEmVigor() {
        return emVigor;
    }

    public void setEmVigor(boolean emVigor) {
        this.emVigor = emVigor;
    }

}
