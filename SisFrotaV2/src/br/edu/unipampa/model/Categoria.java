/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.model;

/**
 *
 * @author JPiagetti
 */
public class Categoria {

    public Categoria(int codCat, boolean situacao) {
        this.codCat = codCat;
        this.situacao = situacao;
    }

    private int codCat;
    private String nomeCategoria;
    private String tipoLinha;
    private double multiplicador;
    private int codPassageiro;
    private boolean situacao;

    public Categoria(String nomeCategoria, String tipoLinha, double multiplicador) {
        this.nomeCategoria = nomeCategoria;
        this.tipoLinha = tipoLinha;
        this.multiplicador = multiplicador;
    }

    public Categoria(int codCat, String nomeCategoria, String tipoLinha, double multiplicador, int codPassageiro) {
        this.codCat = codCat;
        this.nomeCategoria = nomeCategoria;
        this.tipoLinha = tipoLinha;
        this.multiplicador = multiplicador;
        this.codPassageiro = codPassageiro;
    }

    public Categoria() {
    }

    public Categoria(String categoria, String frota, double desconto, boolean situacao) {
        this.nomeCategoria = categoria;
        this.tipoLinha = frota;
        this.multiplicador = desconto;
        this.situacao = situacao;
    }

    public Categoria(int id) {
        this.codCat = id;
    }

    public Categoria(String categoria) {
        this.nomeCategoria = categoria;
    }

    public int getCodCat() {
        return codCat;
    }

    public void setCodCat(int codCat) {
        this.codCat = codCat;
    }

    public String getNomeCategoria() {
        return nomeCategoria;
    }

    public void setNomeCategoria(String nomeCategoria) {
        this.nomeCategoria = nomeCategoria;
    }

    public String getTipoLinha() {
        return tipoLinha;
    }

    public void setTipoLinha(String tipoLinha) {
        this.tipoLinha = tipoLinha;
    }

    public double getMultiplicador() {
        return multiplicador;
    }

    public void setMultiplicador(double multiplicador) {
        this.multiplicador = multiplicador;
    }

    public int getCodPassageiro() {
        return codPassageiro;
    }

    public void setCodPassageiro(int codPassageiro) {
        this.codPassageiro = codPassageiro;
    }

    public boolean isSituacao() {
        return situacao;
    }

    public void setSituacao(boolean situacao) {
        this.situacao = situacao;
    }

}
