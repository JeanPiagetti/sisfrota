/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.model;

import java.util.Date;

/**
 *
 * @author JPiagetti
 */
public class Concessao {

    private int codigo;
    private String partida;
    private String chegada;
    private Date dataInicial;
    private Date dataFinal;
    private String documentoPath;
    private String nomeDocumento;
    private boolean situacao;

    public Concessao() {
    }

    public Concessao(int codigo, String partida, String chegada, Date dataInicial, Date dataFinal, String documentoPath, String nomeDocumento, boolean situacao) {
        this.codigo = codigo;
        this.partida = partida;
        this.chegada = chegada;
        this.dataInicial = dataInicial;
        this.dataFinal = dataFinal;
        this.documentoPath = documentoPath;
        this.nomeDocumento = nomeDocumento;
        this.situacao = situacao;
    }

    public Concessao(String partida, String chegada, Date dataInicial, Date dataFinal, String documentoPath, String nomeDocumento, boolean situacao) {
        this.partida = partida;
        this.chegada = chegada;
        this.dataInicial = dataInicial;
        this.dataFinal = dataFinal;
        this.documentoPath = documentoPath;
        this.nomeDocumento = nomeDocumento;
        this.situacao = situacao;
    }

    public Concessao(String partida, String chegada, Date dataInicial, Date dataFinal, String documentoPath, String nomeDocumento) {
        this.partida = partida;
        this.chegada = chegada;
        this.dataInicial = dataInicial;
        this.dataFinal = dataFinal;
        this.documentoPath = documentoPath;
        this.nomeDocumento = nomeDocumento;
    }

    public boolean isSituacao() {
        return situacao;
    }

    public void setSituacao(boolean situacao) {
        this.situacao = situacao;
    }

    public Concessao(int codigo, String partida, String chegada, Date dataInicial, Date dataFinal, String nomeDocumento, String documentoPath) {
        this.codigo = codigo;
        this.partida = partida;
        this.chegada = chegada;
        this.dataInicial = dataInicial;
        this.dataFinal = dataFinal;
        this.nomeDocumento = nomeDocumento;
        this.documentoPath = documentoPath;
    }

    public String getDocumentoPath() {
        return documentoPath;
    }

    public void setDocumentoPath(String documentoPath) {
        this.documentoPath = documentoPath;
    }

    public String getNomeDocumento() {
        return nomeDocumento;
    }

    public void setNomeDocumento(String nomeDocumento) {
        this.nomeDocumento = nomeDocumento;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getPartida() {
        return partida;
    }

    public void setPartida(String partida) {
        this.partida = partida;
    }

    public String getChegada() {
        return chegada;
    }

    public void setChegada(String chegada) {
        this.chegada = chegada;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public String getSituacao() {
        if (isSituacao()) {
            return "Ativo";
        } else {
            return "Inativo";
        }
    }

}
