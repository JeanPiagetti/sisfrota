/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.model;

import java.sql.Time;
import java.util.Date;

/**
 *
 * @author JPiagetti
 */
public class Passagem {

    private int codPassagem;
    private double valor;
    private String descricao;
    private int codEmp;
    private String linha;
    private int numBox;
    private Date dataViagem;
    private Time hrSaida;
    private Time hrChegada;
    private String origem;
    private String destino;
    private String modalidade;

    public Passagem(int codPassagem) {
        this.codPassagem = codPassagem;
    }

    public Passagem() {
    }

    public Passagem(int codPassagem, double valor, String descricao, int codEmp, String linha, int numBox, Date dataViagem, Time hrSaida, Time hrChegada, String origem, String destino, String modalidade) {
        this.codPassagem = codPassagem;
        this.valor = valor;
        this.descricao = descricao;
        this.codEmp = codEmp;
        this.linha = linha;
        this.numBox = numBox;
        this.dataViagem = dataViagem;
        this.hrSaida = hrSaida;
        this.hrChegada = hrChegada;
        this.origem = origem;
        this.destino = destino;
        this.modalidade = modalidade;
    }

    public Passagem(String destino) {
        this.destino = destino;
    }

    public int getCodPassagem() {
        return codPassagem;
    }

    public void setCodPassagem(int codPassagem) {
        this.codPassagem = codPassagem;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getCodEmp() {
        return codEmp;
    }

    public void setCodEmp(int codEmp) {
        this.codEmp = codEmp;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public int getNumBox() {
        return numBox;
    }

    public void setNumBox(int numBox) {
        this.numBox = numBox;
    }

    public Date getDataViagem() {
        return dataViagem;
    }

    public void setDataViagem(Date dataViagem) {
        this.dataViagem = dataViagem;
    }

    public Date getHrSaida() {
        return hrSaida;
    }

    public void setHrSaida(Time hrSaida) {
        this.hrSaida = hrSaida;
    }

    public Date getHrChegada() {
        return hrChegada;
    }

    public void setHrChegada(Time hrChegada) {
        this.hrChegada = hrChegada;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getModalidade() {
        return modalidade;
    }

    public void setModalidade(String modalidade) {
        this.modalidade = modalidade;
    }

}
