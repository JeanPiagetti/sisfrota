/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.controller;

import br.edu.unipampa.model.Assento;
import br.edu.unipampa.model.Categoria;
import br.edu.unipampa.model.Passagem;
import br.edu.unipampa.persistencia.DAOAssento;
import br.edu.unipampa.persistencia.DAOCategoria;
import br.edu.unipampa.persistencia.DAOVenda;
import java.util.ArrayList;
import java.util.Formatter;

/**
 *
 * @author JPiagetti
 */
public class ControllerVenda {

    private DAOVenda dao;
    private DAOCategoria daoCate;
    private DAOAssento daoAssento;

    public ControllerVenda() {
        dao = new DAOVenda();
        daoCate = new DAOCategoria();
        daoAssento = new DAOAssento();
    }

    public ArrayList<Passagem> preencheTabela() {
//        DAOVenda dao = new DAOVenda();
        return dao.buscarAll();
    }

    public ArrayList<Passagem> buscarLinha(String destino) {
        //      DAOVenda dao = new DAOVenda();
        return dao.buscarArray(new Passagem(destino));
    }

    public Passagem buscar(int id) {
        //    DAOVenda dao = new DAOVenda();
        return dao.buscar(new Passagem(id));
    }

    public double retornaValorTotal(String categoria ,String assento, String destino) {
        Passagem passagem = dao.buscar(new Passagem(destino));
        Categoria cat = daoCate.buscar(new Categoria(categoria));
        Assento assen = daoAssento.buscar(new Assento(assento));
        double valorTotal = (passagem.getValor() * cat.getMultiplicador()) + assen.getTaxaAssento();
        return valorTotal;
    }
//
//    public double retornaValorCategoria(String categoria, String destino) {
//        Passagem passagem = dao.buscar(new Passagem(destino));
//        Categoria cate = daoCate.buscar(new Categoria(categoria));
//
//        double total = passagem.getValor() + cate.getMultiplicador();
//
//        return total;

    public void venderPassagem(String chegada, String assento, String categoria, boolean seguro, String dataViagem, String hrSaida, String hrChegada, double valor) {
        //  DAOVenda dao = new DAOVenda();
        dao.inserir(new Passagem(/*chegada,assento,categoria,seguro,dataViagem,hrSaida,hrChegada,valor*/));
    }

    public static void main(String[] args) {
        ControllerVenda venda = new ControllerVenda();
        double total = venda.retornaValorTotal("Estudante", "Executivo", "Joinville");

        System.out.println(venda.retornaValorTotal("Idoso", "Comum", "Porto alegre"));
        System.out.println(venda.retornaValorTotal("Estudante", "Comum", "Porto alegre"));
        System.out.println(venda.retornaValorTotal("Estudante", "Comum", "Joinville"));
        StringBuffer bf = new StringBuffer();
        Formatter out = new Formatter(bf);
        out.format("%10.2f", total);
        System.out.println("Total com 2 casas " + out);
    }
}
