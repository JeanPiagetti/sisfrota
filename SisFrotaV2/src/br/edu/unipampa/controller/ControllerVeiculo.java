/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.controller;

import br.edu.unipampa.model.Veiculo;
import br.edu.unipampa.persistencia.DAOVeiculo;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author unipampa
 */
public class ControllerVeiculo {

    DAOVeiculo dao;

    public ControllerVeiculo() {

        dao = new DAOVeiculo();
    }

    public void cadastroVeiculo(Veiculo veiculo) {
        dao.inserir(veiculo);
    }

    public void editarVeiculo(Veiculo v) {
        dao.editar(v);
    }

    public ArrayList<Veiculo> preencheTabela() {
        return dao.ler();
    }

    public void excluir(Veiculo v) {
        dao.excluir(v);
    }

    public ArrayList<Veiculo> lerInativos() {
        return dao.lerInativos();
    }

    public ArrayList<Veiculo> lerAtivos() {
        return dao.lerAtivos();
    }

}
