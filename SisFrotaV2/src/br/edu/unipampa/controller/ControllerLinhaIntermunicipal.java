/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.controller;

import br.edu.unipampa.model.LinhaIntermunicipal;
import br.edu.unipampa.persistencia.DAOLinhaIntermunicipal;
import java.util.ArrayList;

/**
 *
 * @author JPiagetti
 */
public class ControllerLinhaIntermunicipal {

    private final DAOLinhaIntermunicipal dao;

    public ControllerLinhaIntermunicipal() {
        this.dao = new DAOLinhaIntermunicipal();
    }

    public void inserirTrecho(String origem, String destino) {
        dao.inserirLinhaIntermunicipal(new LinhaIntermunicipal(origem, destino));
    }

    public ArrayList<LinhaIntermunicipal> preencherTabela() {
        return dao.ler();
    }
    public ArrayList<LinhaIntermunicipal> preencher(String destino){
        return dao.buscarAll(destino);
    } 
}
