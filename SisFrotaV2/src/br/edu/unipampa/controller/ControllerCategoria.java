/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.controller;

import br.edu.unipampa.model.Categoria;
import br.edu.unipampa.persistencia.DAOCategoria;
import java.util.ArrayList;

/**
 *
 * @author JPiagetti
 */
public class ControllerCategoria {

    private final DAOCategoria dao;

    public ControllerCategoria() {
        dao = new DAOCategoria();
    }

    public void cadastraCategoria(String categoria, double desconto, String frota, boolean situacao) {
        //dao = new DAOCategoria();
        dao.inserir(new Categoria(categoria, frota, desconto, situacao));

    }

    public ArrayList<Categoria> preencheTabelaInativos() {
        //  dao = new DAOCategoria();
        return dao.buscarAllInativos();
    }

    public ArrayList<Categoria> preencheTabela() {
        // dao = new DAOCategoria();
        return dao.buscarAll();
    }

    public void removerDados(int id) {
        //  dao = new DAOCategoria();
        if (dao.buscar(new Categoria(id, true)).getCodPassageiro() == 0) {
            dao.excluir(id);
        }

    }

    public double getDesconto(String categoria) {
        double desconto;
        //  dao = new DAOCategoria();
        desconto = dao.buscar(new Categoria(categoria)).getMultiplicador();
        return desconto;
    }

    public void inativarCadastro(int id) {
        //  dao = new DAOCategoria();
        dao.atualizarSituacao(new Categoria(id));
    }

    public void atualizaDados(int id, String nomeCategoria, String linha, String vantagem) {
        double multiplicador;
        if (vantagem.compareToIgnoreCase("Integral") == 0) {
            multiplicador = 0.0;
            //      dao = new DAOCategoria();
            dao.atualizar(new Categoria(id, nomeCategoria, linha, multiplicador, 0));
        } else {
            multiplicador = 0.5;
            //  dao = new DAOCategoria();
            dao.atualizar(new Categoria(id, nomeCategoria, linha, multiplicador, 0));
        }

    }
}
