/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.controller;

import br.edu.unipampa.model.Pagamento;
import br.edu.unipampa.persistencia.DAOPagamento;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author JPiagetti
 */
public class ControllerPagamento {

    public void cadastroPagamento(String nomeFormaPagamento, String tipoPagamento, String linha, boolean situacao) {
        DAOPagamento dao = new DAOPagamento();
        dao.inserir(new Pagamento(nomeFormaPagamento, tipoPagamento, linha, situacao));
    }

    public void editarPagamento(int id, String nomePagamento, String tipoPag, String linha) {
        DAOPagamento dao = new DAOPagamento();
        Pagamento pagamento = new Pagamento(id, nomePagamento, tipoPag, linha);
        dao.atualizar(pagamento);
    }

    public ArrayList<Pagamento> preencherTabela() {
        DAOPagamento dao = new DAOPagamento();
        return dao.buscarAll();
    }

    public ArrayList<Pagamento> preencherTabelaInativos() {
        DAOPagamento dao = new DAOPagamento();
        return dao.buscarAllInativos();
    }

    public void removerDadosTabela(int id) {
        DAOPagamento dao = new DAOPagamento();
        dao.excluir(id);

    }

    public void inativarCadastro(int id) {
        DAOPagamento dao = new DAOPagamento();
        dao.atualizarSituacao(id);
    }
}
