/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.controller;

import br.edu.unipampa.model.Tarifa;
import br.edu.unipampa.persistencia.DAOTarifa;
import java.util.ArrayList;

/**
 *
 * @author Huillian Serpa
 */
public class ControllerTarifa {

    private final DAOTarifa dao;

    public ControllerTarifa() {
        dao = new DAOTarifa();
    }

    public ArrayList<Tarifa> preencheTablela() {
        // dao = new DAOTarifa();
        return dao.buscarAll();
    }

    public double retornaTarifa(String assento) {
        return dao.buscar(new Tarifa(assento)).getValor();
    }

    public boolean excluir(Tarifa tarifa) {
        return dao.excluir(tarifa);
    }

    public boolean inserirTarifa(Tarifa tarifa) {
        return dao.inserir(tarifa);
    }

    public boolean editarTarifa(Tarifa tarifa) {
        return dao.editar(tarifa);
    }

}
