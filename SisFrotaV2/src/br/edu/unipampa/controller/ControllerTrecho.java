/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.controller;

import br.edu.unipampa.model.Trecho;
import br.edu.unipampa.persistencia.DAOTrecho;
import java.util.ArrayList;

/**
 *
 * @author Huillian Serpa
 */
public class ControllerTrecho {

    private DAOTrecho dao;

    public ControllerTrecho() {
        dao = new DAOTrecho();
    }

    public ArrayList<Trecho> preencheTabela() {

        return dao.ler();
    }

    public void inserirTrechos(String origem, String destino, float distancia) {
        Trecho trecho = new Trecho(origem, destino, distancia);
        dao.incluir(trecho);
    }

}
