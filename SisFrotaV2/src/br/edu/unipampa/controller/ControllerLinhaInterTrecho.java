/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.controller;

import br.edu.unipampa.model.LinhaInterTrecho;
import br.edu.unipampa.persistencia.DAOLinhaInterTrecho;
import java.util.ArrayList;

/**
 *
 * @author Huillian Serpa
 */
public class ControllerLinhaInterTrecho {
    private DAOLinhaInterTrecho daoLinha;
    
    public ControllerLinhaInterTrecho(){
        daoLinha = new DAOLinhaInterTrecho();
    }
    
    public boolean inserirLinhaInterTrecho(LinhaInterTrecho linha){
        return daoLinha.inserirLinhaInterTrecho(linha);
    }
    
    public ArrayList<LinhaInterTrecho> preencherTabela(){
        return daoLinha.ler();
    }

}
