/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.controller;

import br.edu.unipampa.model.Concessao;
import br.edu.unipampa.persistencia.DAOConcessao;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author JPiagetti
 */
public class ControllerConcessao {

    public void salvarConcessao(String partida, String chegada, Date data1, Date data2, String nomeDocumento, String documentoPath, boolean situacao) {
        DAOConcessao dao = new DAOConcessao();
        dao.inserir(new Concessao(partida, chegada, data1, data2, nomeDocumento, documentoPath, situacao));

    }

    public ArrayList<Concessao> preencherTabela(boolean situacao) {
        DAOConcessao dao = new DAOConcessao();
        return dao.buscarAll(situacao);
    }

    public Concessao buscarConcessao(int id) {
        DAOConcessao dao = new DAOConcessao();
        return dao.buscar(id);
    }

    public void alterarConcessao(int id, String partida, String chegada, Date data1, Date data2, String nomeDocumento, String documentoPath, boolean situacao) {
        DAOConcessao dao = new DAOConcessao();
        dao.atualizar(new Concessao(id, partida, chegada, data1, data2, nomeDocumento, documentoPath, situacao));
    }

    public void inativarConcessao(int id) {
        DAOConcessao dao = new DAOConcessao();
        dao.atualizarSituacao(id);
    }
}
