/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.persistencia;

import br.edu.unipampa.model.Concessao;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author JPiagetti
 */
public class DAOConcessao {

    public void inserir(Concessao c) {
        Connection con = ConnectionFactory.conectar();
        String sql = "INSERT INTO concessao(codCons,partida,chegada,dataInicio,dataFim,documentoCon,pathDocumento,situacao) VALUES (?,?,?,?,?,?,?,?)";
        PreparedStatement consulta = null;
        try {
            consulta = con.prepareStatement(sql);
            consulta.setString(1, null);
            consulta.setString(2, c.getPartida());
            consulta.setString(3, c.getChegada());
            consulta.setDate(4, new Date(c.getDataInicial().getTime()));
            consulta.setDate(5, new Date(c.getDataFinal().getTime()));//Como vai você?? :)
            consulta.setString(6, c.getDocumentoPath());
            consulta.setString(7, c.getNomeDocumento());
            consulta.setBoolean(8, c.isSituacao());
            consulta.execute();
            consulta.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOConcessao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
    }

    public ArrayList<Concessao> buscarAll(boolean situacao) {
        String sql = null;
        PreparedStatement consulta = null;
        ResultSet rs = null;
        Concessao concessao = null;
        ArrayList<Concessao> concessoes = new ArrayList<>();
        Connection con = ConnectionFactory.conectar();
        try {
            if (situacao) {
                sql = "SELECT *FROM concessao WHERE situacao = true";
                consulta = con.prepareStatement(sql);
                rs = consulta.executeQuery();
                while (rs.next()) {
                    concessao = new Concessao();
                    concessao.setCodigo(rs.getInt("codCons"));
                    concessao.setPartida(rs.getString("partida"));
                    concessao.setChegada(rs.getString("chegada"));
                    concessao.setDataInicial(rs.getDate("dataInicio"));
                    concessao.setDataFinal(rs.getDate("dataFim"));
                    concessao.setNomeDocumento(rs.getString("documentoCon"));
                    concessao.setDocumentoPath(rs.getString("pathDocumento"));
                    concessao.setSituacao(rs.getBoolean("situacao"));
                    concessoes.add(concessao);
                }
            } else {
                sql = "SELECT * FROM concessao WHERE situacao = false";
                consulta = con.prepareStatement(sql);
                rs = consulta.executeQuery();
                while (rs.next()) {
                    concessao = new Concessao();
                    concessao.setCodigo(rs.getInt("codCons"));
                    concessao.setPartida(rs.getString("partida"));
                    concessao.setChegada(rs.getString("chegada"));
                    concessao.setDataInicial(rs.getDate("dataInicio"));
                    concessao.setDataFinal(rs.getDate("dataFim"));
                    concessao.setNomeDocumento(rs.getString("documentoCon"));
                    concessao.setDocumentoPath(rs.getString("pathDocumento"));
                    concessao.setSituacao(rs.getBoolean("situacao"));
                    concessoes.add(concessao);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOConcessao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return concessoes;
    }

    public ArrayList<Concessao> buscarAllInativos() {
        Connection con = ConnectionFactory.conectar();
        String sql = "SELECT * FROM concessao WHERE situacao = false";
        PreparedStatement consulta = null;
        ResultSet rs = null;
        Concessao concessao = null;
        ArrayList<Concessao> concessoes = new ArrayList<>();
        try {
            consulta = con.prepareStatement(sql);
            rs = consulta.executeQuery();
            while (rs.next()) {
                concessao = new Concessao();
                concessao.setCodigo(rs.getInt("codCons"));
                concessao.setPartida(rs.getString("partida"));
                concessao.setChegada(rs.getString("chegada"));
                concessao.setDataInicial(rs.getDate("dataInicio"));
                concessao.setDataFinal(rs.getDate("dataFim"));
                concessao.setNomeDocumento(rs.getString("documentoCon"));
                concessao.setDocumentoPath(rs.getString("pathDocumento"));
                concessao.setSituacao(rs.getBoolean("situacao"));
                concessoes.add(concessao);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOConcessao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return concessoes;
    }

    public Concessao buscar(int id) {
        Connection conexao = ConnectionFactory.conectar();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        Concessao concessao = null;
        String sql = "SELECT * FROM concessao WHERE codCons = ?";
        try {
            consulta = conexao.prepareStatement(sql);
            consulta.setInt(1, id);
            resultado = consulta.executeQuery();
            if (resultado.next()) {
                concessao = new Concessao();
                concessao.setPartida(resultado.getString("partida"));
                concessao.setChegada(resultado.getString("chegada"));
                concessao.setDataInicial(resultado.getDate("dataInicio"));
                concessao.setDataFinal(resultado.getDate("dataFim"));
                concessao.setNomeDocumento(resultado.getString("documentoCon"));
                concessao.setDocumentoPath(resultado.getString("pathDocumento"));
                concessao.setSituacao(resultado.getBoolean("situacao"));
            }
        } catch (SQLException e) {
            Logger.getLogger(DAOConcessao.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return concessao;
    }

    public void atualizar(Concessao c) {
        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;
        String sql = "UPDATE sisfrota.concessao SET partida = ?, chegada = ?, dataInicio = ?,dataFim = ? , documentoCon = ?, pathDocumento = ?,situacao = ?   WHERE codCons = ?";
        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, c.getPartida());
            stmt.setString(2, c.getChegada());
            stmt.setDate(3, new Date(c.getDataInicial().getTime()));
            stmt.setDate(4, new Date(c.getDataFinal().getTime()));
            stmt.setString(5, c.getNomeDocumento());
            stmt.setString(6, c.getDocumentoPath());
            stmt.setBoolean(7, c.isSituacao());
            stmt.setInt(8, c.getCodigo());
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Atualizado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao atualizar: " + ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }

    }

    public void atualizarSituacao(int id) {
        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("UPDATE concessao SET situacao = false  where codCons = ?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Inativado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro : " + ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }

    }

}
