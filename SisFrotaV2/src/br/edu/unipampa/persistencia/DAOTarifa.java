/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.persistencia;

import br.edu.unipampa.model.Tarifa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JPiagetti
 */
public class DAOTarifa {

    //Tarifa tarifa = null;
    public boolean inserir(Tarifa tarifa) {
        Connection con = ConnectionFactory.conectar();
        String sql = "INSERT INTO sisfrota.tarifa (emVigor,valor, tipoTar) VALUES (?,?,?);";
        PreparedStatement stmt = null;
        try {

            stmt = con.prepareStatement(sql);
            stmt.setBoolean(1, tarifa.isEmVigor());
            stmt.setDouble(2, tarifa.getValor());
            stmt.setString(3, tarifa.getTipoTarifa());

            stmt.executeUpdate();
            stmt.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DAOPagamento.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            ConnectionFactory.fecharConexao();
        }
    }

    public boolean editar(Tarifa tarifa) {
        Connection con = ConnectionFactory.conectar();
        String sql = "UPDATE sisfrota.tarifa SET emVigor=?,valor=?, tipoTar=? WHERE codTar=?;";
        PreparedStatement stmt = null;
        try {

            stmt = con.prepareStatement(sql);

            stmt.setBoolean(1, tarifa.isEmVigor());
            stmt.setDouble(2, tarifa.getValor());
            stmt.setString(3, tarifa.getTipoTarifa());
            stmt.setInt(4, tarifa.getCodTarifa());

            stmt.executeUpdate();
            stmt.close();
            con.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DAOPagamento.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("O PROBLEMA É:" + ex.getMessage());
            return false;
        } finally {
            ConnectionFactory.fecharConexao();
        }
    }

    public ArrayList<Tarifa> buscarAll() {
        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<Tarifa> tarifas = new ArrayList<>();

        try {

            stmt = con.prepareStatement("SELECT * FROM sisfrota.tarifa");
            rs = stmt.executeQuery();

            while (rs.next()) {

                Tarifa tarifa = new Tarifa();
                tarifa.setCodTarifa(rs.getInt("codTar"));
                tarifa.setValor(rs.getFloat("valor"));
                tarifa.setEmVigor(rs.getBoolean("emVigor"));
                tarifa.setTipoTarifa(rs.getString("tipoTar"));
                tarifas.add(tarifa);
            }

        } catch (SQLException ex) {
            System.out.println("O problema é:" + ex);
        } finally {
            try {
                //fechar conexao
                con.close();
            } catch (SQLException ex) {
                System.out.println("Problema ao fechar conexão.\n" + ex.getMessage());
            }
        }
        return tarifas;
    }

    public boolean excluir(Tarifa tarifa) {
        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("DELETE FROM sisfrota.tarifa WHERE codTar=?");

            stmt.setInt(1, tarifa.getCodTarifa());
            stmt.executeUpdate();

            stmt.close();

            return true;

        } catch (SQLException ex) {
            System.out.println("O problema é:" + ex);
            return false;
        } finally {
            try {
                //fechar conexao
                con.close();
            } catch (SQLException ex) {
                System.out.println("Problema ao fechar conexão.\n" + ex.getMessage());
            }
        }
    }

    public Tarifa buscar(Tarifa t) {
        Connection conexao = ConnectionFactory.conectar();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        Tarifa tarifa = null;
        String sql = "SELECT * FROM tarifa WHERE codTar= ? OR tipoTari=?";
        //caso precisar fazer um select dinâmico
        try {
            consulta = conexao.prepareStatement(sql);
            consulta.setInt(1, t.getCodTarifa());
            consulta.setString(2, t.getTipoTarifa());
            resultado = consulta.executeQuery();
            if (resultado.next()) {
                tarifa = new Tarifa();
                tarifa.setCodTarifa(resultado.getInt("codTar"));
                tarifa.setValor(resultado.getFloat("valor"));
                tarifa.setTipoTarifa(resultado.getString("tipoTari"));
                tarifa.setEmVigor(resultado.getBoolean("isVigor"));

            }
        } catch (SQLException e) {
            Logger.getLogger(DAOTarifa.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return tarifa;
    }
}
