/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.persistencia;

import br.edu.unipampa.model.LinhaIntermunicipal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Huillian Serpa
 */
public class DAOLinhaIntermunicipal {

    public boolean inserirLinhaIntermunicipal(LinhaIntermunicipal linha) {

        Connection con = ConnectionFactory.conectar();
        String sql = "INSERT INTO sisfrota.linhaintermunicipal (origem,destino) VALUES (?,?);";
        PreparedStatement stmt = null;
        //String trecho=null; 

        try {

            //DAOTrecho dao = new DAOTrecho();
            stmt = con.prepareStatement(sql);
            stmt.setString(1, linha.getOrigem());
            stmt.setString(2, linha.getDestino());
//            for (int i = 0; i < trechos.length; i++) {
//                trecho = trechos[i]+","; 
//            }
//            stmt.setString(3, trecho);

            stmt.executeUpdate();
            stmt.close();
            return true;
        } catch (SQLException ex) {
            System.out.println("erro:" + ex.getMessage());

            Logger.getLogger(DAOPagamento.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            ConnectionFactory.fecharConexao();
        }
    }

    public ArrayList<LinhaIntermunicipal> ler() {
        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<LinhaIntermunicipal> linhasIntermunicipais = new ArrayList<>();

        try {

            stmt = con.prepareStatement("SELECT * FROM sisfrota.linhaIntermunicipal");
            rs = stmt.executeQuery();

            while (rs.next()) {

                LinhaIntermunicipal linhaInter = new LinhaIntermunicipal();
                linhaInter.setCodigo(rs.getInt("codigo"));
                linhaInter.setOrigem(rs.getString("origem"));
                linhaInter.setDestino(rs.getString("destino"));
                linhasIntermunicipais.add(linhaInter);
            }

        } catch (SQLException ex) {
            System.out.println("O problema é:" + ex);
        } finally {
            try {
                //fechar conexao
                con.close();
            } catch (SQLException ex) {
                System.out.println("Problema ao fechar conexão.\n" + ex.getMessage());
            }
        }
        return linhasIntermunicipais;
    }

    public int codigoLinha(LinhaIntermunicipal linha) {
        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;
        ResultSet rs;
        String origem = linha.getOrigem();
        String destino = linha.getDestino();
        int codigo = 0;
        String sql = "SELECT codigo FROM linhaintermunicipal WHERE codigo=" + origem + " AND codigo=" + destino;
        try {

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                linha.setCodigo(rs.getInt("codigo"));
            }

            return codigo;

        } catch (SQLException ex) {
            Logger.getLogger(DAOLinhaIntermunicipal.class.getName()).log(Level.SEVERE, null, ex);

            return -1;
        }
    }

    public ArrayList<LinhaIntermunicipal> buscarAll(String destino) {
        Connection conexao = ConnectionFactory.conectar();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        LinhaIntermunicipal linha = null;
        String sql = "SELECT * FROM linhaintermunicipal WHERE destino= ?";
        ArrayList<LinhaIntermunicipal> linhas = new ArrayList<>();
        try {
            consulta = conexao.prepareStatement(sql);
            consulta.setString(1, destino);
            resultado = consulta.executeQuery();
            while (resultado.next()) {
                linha = new LinhaIntermunicipal();
                linha.setCodigo(resultado.getInt("codigo"));
                linha.setOrigem(resultado.getString("origem"));
                linha.setDestino(resultado.getString("destino"));
                linhas.add(linha);

            }
        } catch (SQLException e) {
            Logger.getLogger(DAOLinhaIntermunicipal.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return linhas;
    }

}
