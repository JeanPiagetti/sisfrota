/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.persistencia;

import br.edu.unipampa.model.Passagem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JPiagetti
 */
public class DAOVenda {

    public ArrayList<Passagem> buscarAll() {
        Connection con = ConnectionFactory.conectar();
        String sql = "SELECT * FROM passagem";
        PreparedStatement consulta = null;
        ResultSet rs = null;
        Passagem passagem = null;
        ArrayList<Passagem> passagens = new ArrayList<>();
        try {
            consulta = con.prepareStatement(sql);
            rs = consulta.executeQuery();
            while (rs.next()) {
                passagem = new Passagem();
                passagens.add(passagem);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOVenda.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return passagens;
    }

    public void deletar(int id) {

    }

    public void inserir(Passagem p) {
        Connection conexao = ConnectionFactory.conectar();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        Passagem passagem = null;
        String sql = "INSERT INTO venda VALUES()";

    }

    public Passagem buscar(Passagem p) {
        Connection conexao = ConnectionFactory.conectar();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        Passagem passagem = null;
        String sql = "SELECT * FROM passagem WHERE codPassagem= ? OR destino = ?";
        //caso precisar fazer um select dinâmico
        try {
            consulta = conexao.prepareStatement(sql);
            consulta.setInt(1, p.getCodPassagem());
            consulta.setString(2, p.getDestino());

            resultado = consulta.executeQuery();
            if (resultado.next()) {
                passagem = new Passagem();
                passagem.setCodEmp(resultado.getInt("codEmp"));
                passagem.setCodPassagem(resultado.getInt("codPassagem"));
                passagem.setDataViagem(resultado.getDate("dataViagem"));
                passagem.setOrigem(resultado.getString("origem"));
                passagem.setDestino(resultado.getString("destino"));
                passagem.setHrSaida(resultado.getTime("hrSaida"));
                passagem.setHrChegada(resultado.getTime("hrChegada"));
                passagem.setLinha(resultado.getString("linha"));
                passagem.setModalidade(resultado.getString("modalidade"));
                passagem.setNumBox(resultado.getInt("numBox"));
                passagem.setValor(resultado.getDouble("valor"));
            }
        } catch (SQLException e) {
            Logger.getLogger(DAOConcessao.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return passagem;
    }

    public ArrayList<Passagem> buscarArray(Passagem p) {
        Connection conexao = ConnectionFactory.conectar();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        Passagem passagem = null;
        String sql = "SELECT * FROM passagem WHERE destino= ?";
        ArrayList<Passagem> passagens = new ArrayList<>();
        try {
            consulta = conexao.prepareStatement(sql);
            consulta.setString(1, p.getDestino());
            resultado = consulta.executeQuery();
            while (resultado.next()) {
                passagem = new Passagem();
                passagem.setCodEmp(resultado.getInt("codEmp"));
                passagem.setCodPassagem(resultado.getInt("codPassagem"));
                passagem.setDataViagem(resultado.getDate("dataViagem"));
                passagem.setOrigem(resultado.getString("origem"));
                passagem.setDestino(resultado.getString("destino"));
                passagem.setHrSaida(resultado.getTime("hrSaida"));
                passagem.setHrChegada(resultado.getTime("hrChegada"));
                passagem.setLinha(resultado.getString("linha"));
                passagem.setModalidade(resultado.getString("modalidade"));
                passagem.setNumBox(resultado.getInt("numBox"));
                passagem.setValor(resultado.getDouble("valor"));
                passagens.add(passagem);

            }
        } catch (SQLException e) {
            Logger.getLogger(DAOConcessao.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return passagens;
    }

}
