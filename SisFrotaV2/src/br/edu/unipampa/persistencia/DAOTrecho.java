/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.persistencia;

import br.edu.unipampa.model.Trecho;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Huillian Serpa
 */
public class DAOTrecho {

    public boolean incluir(Trecho trecho) {
        Connection con = ConnectionFactory.conectar();
        String sql = "INSERT INTO sisfrota.trecho (origem,destino, distancia) VALUES (?,?,?);";
        PreparedStatement stmt = null;
        try {

            stmt = con.prepareStatement(sql);
            stmt.setString(1, trecho.getOrigem());
            stmt.setString(2, trecho.getDestino());
            stmt.setFloat(3, trecho.getDistancia());

            stmt.executeUpdate();
            stmt.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DAOPagamento.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            ConnectionFactory.fecharConexao();
        }
    }

    public ArrayList<Trecho> ler() {
        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<Trecho> trechos = new ArrayList<>();
        try {

            stmt = con.prepareStatement("SELECT * FROM sisfrota.trecho");
            rs = stmt.executeQuery();

            while (rs.next()) {

                Trecho t = new Trecho();

                t.setCodigo(rs.getInt("codTrecho"));
                t.setOrigem(rs.getString("origem"));
                t.setDestino(rs.getString("destino"));
                t.setDistancia(rs.getFloat("distancia"));

                trechos.add(t);
            }

        } catch (SQLException ex) {
            System.out.println("O problema é:" + ex);
        } finally {
            try {
                //fechar conexao
                con.close();
            } catch (SQLException ex) {
                System.out.println("Problema ao fechar conexão.\n" + ex.getMessage());
            }
        }
        return trechos;
    }

    public boolean editar(Trecho trecho) {
        Connection con = ConnectionFactory.conectar();
        String sql = "UPDATE sisfrota.trecho SET origem=?,destino=?, distancia=? WHERE codigo=?;";
        PreparedStatement stmt = null;
        try {

            stmt = con.prepareStatement(sql);

            stmt.setString(1, trecho.getOrigem());
            stmt.setString(2, trecho.getDestino());
            stmt.setFloat(3, trecho.getDistancia());
            stmt.setInt(4, trecho.getCodigo());

            stmt.executeUpdate();
            stmt.close();
            con.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DAOPagamento.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("O PROBLEMA É:" + ex.getMessage());
            return false;
        } finally {
            ConnectionFactory.fecharConexao();
        }
    }

    public boolean excluir(Trecho trecho) {

        //pegar conexao atraves de metodo statico da classe conexao
        //Connection con = ConnectionFactory.getConnection();
        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("DELETE FROM sisfrota.trecho WHERE codigo=?");

            stmt.setInt(1, trecho.getCodigo());
            stmt.executeUpdate();

            stmt.close();

            return true;

        } catch (SQLException ex) {
            System.out.println("O problema é:" + ex);
            return false;
        } finally {
            try {
                //fechar conexao
                con.close();
            } catch (SQLException ex) {
                System.out.println("Problema ao fechar conexão.\n" + ex.getMessage());
            }
        }

    }

}
