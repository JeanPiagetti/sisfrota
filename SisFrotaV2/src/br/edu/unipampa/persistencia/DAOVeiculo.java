package br.edu.unipampa.persistencia;

import br.edu.unipampa.model.Veiculo;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class DAOVeiculo {

    public void inserir(Veiculo veiculo) {
        PreparedStatement stmt;
        Connection con = null;
        try {
            con = ConnectionFactory.conectar();
            stmt = con.prepareStatement("INSERT INTO veiculo(chassis, placa,"
                    + "marca, modelo,cor, anoFab, anoModel, cidade, quilometragem,"
                    + "situacao, combustivel, renavan, numMotor, tipoVeic,capacidade, numAssentos, motivo) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            stmt.setString(1, veiculo.getChassis());
            stmt.setString(2, veiculo.getPlaca());
            stmt.setString(3, veiculo.getMarca());
            stmt.setString(4, veiculo.getModelo());
            stmt.setString(5, veiculo.getCor());
            java.sql.Date dataSql = new java.sql.Date(veiculo.getAnoFab().getTime());
            stmt.setDate(6, dataSql);
            java.sql.Date dataSql2 = new java.sql.Date(veiculo.getAnoModel().getTime());
            stmt.setDate(7, dataSql2);
            stmt.setString(8, veiculo.getCidade());
            stmt.setFloat(9, veiculo.getQuilometragem());
            stmt.setBoolean(10, veiculo.isAtivo());
            stmt.setFloat(11, veiculo.getCombustivel());
            stmt.setString(12, veiculo.getRenavan());
            stmt.setString(13, veiculo.getNumMotor());
            stmt.setString(14, veiculo.getTipoVeiculo());
            stmt.setInt(15, veiculo.getCapacidade());
            stmt.setInt(16, veiculo.getNumAssentos());
            stmt.setString(17, veiculo.getMotivo());

            stmt.executeUpdate();

            JOptionPane.showMessageDialog(null, "Salvo com sucesso");

        } catch (SQLException ex) {
            System.out.println("O problema é:" + ex.getMessage());
        } finally {
            try {
                //fechar conexao
                con.close();
            } catch (SQLException ex) {
                System.out.println("Problema ao fechar conexão.\n" + ex.getMessage());
            }
        }
    }

    public boolean editar(Veiculo veiculo) {

        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;
        String sql = "UPDATE sisfrota.veiculo SET chassis=?, placa=?, marca=?, modelo=?,"
                + " cor=? anoFab=?, anoModel=? cidade=?, quilometragem=?, "
                + "situacao=?, combustivel=? renavan=?, numMotor=?, tipoVeic=?, "
                + "capacidade=?, numAssentos=?, motivo=? where codigoVeic=?";

        try {

            stmt = con.prepareStatement(sql);

            stmt.setString(1, veiculo.getChassis());
            stmt.setString(2, veiculo.getPlaca());
            stmt.setString(3, veiculo.getMarca());
            stmt.setString(4, veiculo.getModelo());
            stmt.setString(5, veiculo.getCor());
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            java.sql.Date dataSql = new java.sql.Date(veiculo.getAnoFab().getTime());

            stmt.setDate(6, dataSql);
            java.sql.Date dataSql2 = new java.sql.Date(veiculo.getAnoModel().getTime());
            stmt.setDate(7, dataSql2);
            stmt.setString(8, veiculo.getCidade());
            stmt.setFloat(9, veiculo.getQuilometragem());
            stmt.setBoolean(10, veiculo.isAtivo());
            stmt.setFloat(11, veiculo.getCombustivel());
            stmt.setString(12, veiculo.getRenavan());
            stmt.setString(13, veiculo.getNumMotor());
            stmt.setString(14, veiculo.getTipoVeiculo());
            stmt.setInt(15, veiculo.getCapacidade());
            stmt.setInt(16, veiculo.getNumAssentos());
            stmt.setString(17, veiculo.getMotivo());
            stmt.setInt(18, veiculo.getCodVeiculo());

            stmt.executeUpdate();
            stmt.close();
            con.close();
            return true;

        } catch (SQLException ex) {
            System.out.println("O problema é:" + ex);
            return false;
        } finally {
            try {
                //fechar conexao
                con.close();
            } catch (SQLException ex) {
                System.out.println("Problema ao fechar conexão.\n" + ex.getMessage());
            }
        }
    }

    public ArrayList<Veiculo> ler() {

        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        ArrayList<Veiculo> veiculos = new ArrayList<>();

        try {
            stmt = con.prepareStatement("SELECT * FROM veiculo");
            rs = stmt.executeQuery();

            while (rs.next()) {
                Veiculo veiculo = new Veiculo();
                veiculo.setCodVeiculo(rs.getInt("codigoVeic"));
                veiculo.setChassis(rs.getString("chassis"));
                veiculo.setPlaca(rs.getString("placa"));
                veiculo.setMarca(rs.getString("marca"));
                veiculo.setModelo(rs.getString("modelo"));
                veiculo.setCor(rs.getString("cor"));
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                veiculo.setAnoFab(rs.getDate("anoFab"));

                veiculo.setAnoModel(rs.getDate("anoModel"));
                veiculo.setCidade(rs.getString("cidade"));
                veiculo.setQuilometragem(rs.getInt("quilometragem"));
                veiculo.setSituacao(rs.getBoolean("situacao"));
                veiculo.setCombustivel(rs.getFloat("combustivel"));
                veiculo.setRenavan(rs.getString("renavan"));
                veiculo.setNumMotor(rs.getString("numMotor"));
                veiculo.setTipoVeiculo(rs.getString("tipoVeic"));
                veiculo.setCapacidade(rs.getInt("capacidade"));
                veiculo.setNumAssentos(rs.getInt("numAssentos"));
                veiculo.setMotivo(rs.getString("motivo"));
                veiculos.add(veiculo);
            }
        } catch (SQLException ex) {
            System.out.println("O problema é:" + ex);
        } finally {
            try {
                //fechar conexao
                con.close();
            } catch (SQLException ex) {
                System.out.println("Problema ao fechar conexão.\n" + ex.getMessage());
            }
        }
        return veiculos;
    }

    public ArrayList<Veiculo> lerInativos() {
        Connection con = ConnectionFactory.conectar();
        String sql = "SELECT * FROM sisfrota.veiculo WHERE situacao = false";
        PreparedStatement consulta = null;
        ResultSet rs = null;
        Veiculo veiculo = null;
        ArrayList<Veiculo> veiculosInativos = new ArrayList<>();
        try {
            consulta = con.prepareStatement(sql);

            rs = consulta.executeQuery();
            while (rs.next()) {
                veiculo = new Veiculo();
                veiculo.setCodVeiculo(rs.getInt("codigoVeic"));
                veiculo.setChassis(rs.getString("chassis"));
                veiculo.setPlaca(rs.getString("placa"));
                veiculo.setMarca(rs.getString("marca"));
                veiculo.setModelo(rs.getString("modelo"));
                veiculo.setCor(rs.getString("cor"));
                veiculo.setAnoFab(rs.getDate("anoFab"));
                veiculo.setAnoModel(rs.getDate("anoModel"));
                veiculo.setCidade(rs.getString("cidade"));
                veiculo.setQuilometragem(rs.getInt("quilometragem"));
                veiculo.setSituacao(rs.getBoolean("situacao"));
                veiculo.setCombustivel(rs.getFloat("combustivel"));
                veiculo.setRenavan(rs.getString("renavan"));
                veiculo.setNumMotor(rs.getString("numMotor"));
                veiculo.setTipoVeiculo(rs.getString("tipoVeic"));
                veiculo.setCapacidade(rs.getInt("capacidade"));
                veiculo.setNumAssentos(rs.getInt("numAssentos"));
                veiculo.setMotivo(rs.getString("motivo"));
                veiculosInativos.add(veiculo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOVeiculo.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return veiculosInativos;
    }

    public ArrayList<Veiculo> lerAtivos() {
        Connection con = ConnectionFactory.conectar();
        String sql = "SELECT * FROM sisfrota.veiculo WHERE situacao = true";
        PreparedStatement consulta = null;
        ResultSet rs = null;
        Veiculo veiculo = null;
        ArrayList<Veiculo> veiculosAtivos = new ArrayList<>();
        try {
            consulta = con.prepareStatement(sql);
            rs = consulta.executeQuery();
            while (rs.next()) {
                veiculo = new Veiculo();
                veiculo.setCodVeiculo(rs.getInt("codigoVeic"));
                veiculo.setChassis(rs.getString("chassis"));
                veiculo.setPlaca(rs.getString("placa"));
                veiculo.setMarca(rs.getString("marca"));
                veiculo.setModelo(rs.getString("modelo"));
                veiculo.setCor(rs.getString("cor"));
                veiculo.setAnoFab(rs.getDate("anoFab"));
                veiculo.setAnoModel(rs.getDate("anoModel"));
                veiculo.setCidade(rs.getString("cidade"));
                veiculo.setQuilometragem(rs.getInt("quilometragem"));
                veiculo.setSituacao(rs.getBoolean("situacao"));
                veiculo.setCombustivel(rs.getFloat("combustivel"));
                veiculo.setRenavan(rs.getString("renavan"));
                veiculo.setNumMotor(rs.getString("numMotor"));
                veiculo.setTipoVeiculo(rs.getString("tipoVeic"));
                veiculo.setCapacidade(rs.getInt("capacidade"));
                veiculo.setNumAssentos(rs.getInt("numAssentos"));
                veiculo.setMotivo(rs.getString("motivo"));
                veiculosAtivos.add(veiculo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOVeiculo.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return veiculosAtivos;
    }

    public void excluir(Veiculo veiculo) {

        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("DELETE FROM veiculo WHERE codVeiculo=?");

            stmt.setInt(0, veiculo.getCodVeiculo());
            stmt.executeUpdate();

            JOptionPane.showMessageDialog(null, "Excluído com sucesso");

        } catch (SQLException ex) {
            System.out.println("O problema é:" + ex);
        } finally {
            try {
                //fechar conexao
                con.close();
            } catch (SQLException ex) {
                System.out.println("Problema ao fechar conexão.\n" + ex.getMessage());
            }
        }
    }
}
