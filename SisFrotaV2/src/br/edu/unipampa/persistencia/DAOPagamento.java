/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.persistencia;

import br.edu.unipampa.model.Pagamento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author JPiagetti
 */
public class DAOPagamento {

    public void inserir(Pagamento p) {
        Connection con = ConnectionFactory.conectar();
        String sql = "INSERT INTO sisfrota.pagamento (`linha`,`nomePag`,`tipoPag`,situacao)VALUES (?,?,?,?)";
        PreparedStatement consulta = null;
        try {
            consulta = con.prepareStatement(sql);
            consulta.setString(1, p.getLinha());
            consulta.setString(2, p.getNomeFormaPagamento());
            consulta.setString(3, p.getTipoPagamento());
            consulta.setBoolean(4, p.isSituacao());
            consulta.executeUpdate();
            consulta.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOPagamento.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
    }

    public Pagamento buscar(int id) {
        Connection conexao = ConnectionFactory.conectar();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        Pagamento pagamento = null;
        String sql = "SELECT * from pagamento where codPag = ?";

        try {
            consulta = conexao.prepareStatement(sql);
            consulta.setInt(1, id);
            resultado = consulta.executeQuery();

            if (resultado.next()) {
                pagamento = new Pagamento();
                pagamento.setIdPagamento(resultado.getInt("codPag"));
                pagamento.setNomeFormaPagamento(resultado.getString("nomePag"));
                pagamento.setTipoPagamento(resultado.getString("tipoPag"));
                pagamento.setLinha(resultado.getString("linha"));
                pagamento.setSituacao(resultado.getBoolean("situacao"));
            }
        } catch (SQLException e) {
            Logger.getLogger(DAOPagamento.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return pagamento;
    }

    public ArrayList<Pagamento> buscarAll() {
        Connection conexao = ConnectionFactory.conectar();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        Pagamento pagamento = null;
        ArrayList<Pagamento> pagamentos = new ArrayList<>();
        String sql = "SELECT * from pagamento WHERE situacao = true";

        try {
            consulta = conexao.prepareStatement(sql);
            resultado = consulta.executeQuery();
            while (resultado.next()) {
                pagamento = new Pagamento();
                //recupera todos os dados do banco e monta os objetos e insere numa lista
                pagamento.setIdPagamento(resultado.getInt("codPag"));
                pagamento.setNomeFormaPagamento(resultado.getString("nomePag"));
                pagamento.setTipoPagamento(resultado.getString("tipoPag"));
                pagamento.setLinha(resultado.getString("linha"));
                pagamentos.add(pagamento);
            }
        } catch (SQLException ex) {
            ConnectionFactory.fecharConexao();
            Logger.getLogger(DAOPagamento.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pagamentos;
    }

    public void atualizar(Pagamento pagamento) {
        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;
        String sql = "UPDATE sisfrota.pagamento SET `nomePag` = ? ,tipoPag=?, linha=? WHERE `codPag` = ?";
        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, pagamento.getNomeFormaPagamento());
            stmt.setString(2, pagamento.getTipoPagamento());
            stmt.setString(3, pagamento.getLinha());
            stmt.setInt(4, pagamento.getIdPagamento());
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Atualizado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao atualizar: " + ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
    }

    public void excluir(int id) {
        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("DELETE FROM pagamento WHERE codPag = ?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Excluido com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir: " + ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
    }

    public void atualizarSituacao(int id) {
        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("UPDATE pagamento SET situacao = false  where codPag = ?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Inativado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro : " + ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }

    }

    public ArrayList<Pagamento> buscarAllInativos() {
        Connection conexao = ConnectionFactory.conectar();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        Pagamento pagamento = null;
        ArrayList<Pagamento> pagamentos = new ArrayList<>();
        String sql = "SELECT * from pagamento WHERE situacao = false";

        try {
            consulta = conexao.prepareStatement(sql);
            resultado = consulta.executeQuery();
            while (resultado.next()) {
                pagamento = new Pagamento();
                //recupera todos os dados do banco e monta os objetos e insere numa lista
                pagamento.setIdPagamento(resultado.getInt("codPag"));
                pagamento.setNomeFormaPagamento(resultado.getString("nomePag"));
                pagamento.setTipoPagamento(resultado.getString("tipoPag"));
                pagamento.setLinha(resultado.getString("linha"));
                pagamento.setSituacao(resultado.getBoolean("situacao"));
                pagamentos.add(pagamento);
            }
        } catch (SQLException ex) {
            ConnectionFactory.fecharConexao();
            Logger.getLogger(DAOPagamento.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pagamentos;

    }
}
