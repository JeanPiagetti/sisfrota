/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.persistencia;

import br.edu.unipampa.model.LinhaInterTrecho;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Huillian Serpa
 */
public class DAOLinhaInterTrecho {

    public boolean inserirLinhaInterTrecho(LinhaInterTrecho linhaInterTrecho) {
        Connection con = ConnectionFactory.conectar();
        String sql = "INSERT INTO sisfrota.linhainterTrecho (codigoLinhaInter,codigoTrecho, descricao) VALUES (?,?,?);";
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, linhaInterTrecho.getCodLinhaInter());
            stmt.setInt(2, linhaInterTrecho.getCodTrecho());
            stmt.setString(3, linhaInterTrecho.getDescricao());

            stmt.executeUpdate();
            stmt.close();

            return true;

        } catch (Exception e) {
            System.out.println("O ERRO EH:" + e);
            return false;

        } finally {
            ConnectionFactory.fecharConexao();

        }
    }

    public ArrayList<LinhaInterTrecho> ler() {
        ArrayList<LinhaInterTrecho> linhasInterTrechos = new ArrayList<>();
        Connection con = ConnectionFactory.conectar();
        ResultSet rs = null;
        PreparedStatement stmt = null;

        try {
            String sql = "SELECT * FROM sisfrota.linhainterTrecho";
            rs = stmt.executeQuery();
            stmt = con.prepareStatement(sql);
            while (rs.next()) {
                LinhaInterTrecho linhaInterTrecho = new LinhaInterTrecho();
                //linhaInterTrecho.set(rs.getInt("cod"));
                linhaInterTrecho.setCodLinhaInter(rs.getInt("codigoLinhaInter"));
                linhaInterTrecho.setCodTrecho(rs.getInt("codigoTrecho"));
                linhaInterTrecho.setDescricao(rs.getString("descricao"));
                linhasInterTrechos.add(linhaInterTrecho);
            }
            stmt.executeUpdate();
            stmt.close();

        } catch (Exception e) {
            System.out.println("O ERRO EH:" + e);

        } finally {
            ConnectionFactory.fecharConexao();

        }
        return linhasInterTrechos;
    }

}
