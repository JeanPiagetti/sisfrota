/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.persistencia;

import br.edu.unipampa.model.Assento;
import br.edu.unipampa.model.Categoria;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JPiagetti
 */
public class DAOAssento {

    public void inserir(Assento assento) {
        Connection con = ConnectionFactory.conectar();
        String sql = "INSERT INTO assento(codAssento,tipoAssento,situacao,codVeiculo,taxaAssento) VALUES (?,?,?,?,?)";
        PreparedStatement consulta = null;
        try {
            consulta = con.prepareStatement(sql);
            consulta.setString(1, null);
            consulta.setString(2, assento.getTipoAssento());
            consulta.setBoolean(3, assento.isSituacao());
            consulta.setInt(4, assento.getCodVeiculo());
            consulta.setDouble(5, assento.getTaxaAssento());
            consulta.execute();
            consulta.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOAssento.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
    }

    public Assento buscar(Assento assento) {
        Connection conexao = ConnectionFactory.conectar();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        //Assento assento = null;
        String sql = "SELECT * FROM assento WHERE codAssento = ? OR tipoAssento=?";
        try {
            consulta = conexao.prepareStatement(sql);
            consulta.setInt(1, assento.getCodAssento());
            consulta.setString(2, assento.getTipoAssento());
            resultado = consulta.executeQuery();
            if (resultado.next()) {
                assento = new Assento();
                assento.setCodAssento(resultado.getInt("codAssento"));
                assento.setTipoAssento(resultado.getString("tipoAssento"));
                assento.setCodVeiculo(resultado.getInt("codVeiculo"));
                assento.setSituacao(resultado.getBoolean("codAssento"));
                assento.setTaxaAssento(resultado.getDouble("taxaAssento"));

            }
        } catch (SQLException e) {
            Logger.getLogger(DAOAssento.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return assento;
    }

    public ArrayList<Assento> buscarAll() {
        Connection con = ConnectionFactory.conectar();
        String sql = "SELECT * FROM assento";
        PreparedStatement consulta = null;
        ResultSet rs = null;
        Assento assento = null;
        ArrayList<Assento> assentos = new ArrayList<>();
        try {
            consulta = con.prepareStatement(sql);
            rs = consulta.executeQuery();
            while (rs.next()) {
                assento = new Assento();
                assento.setCodAssento(rs.getInt("codAssento"));
                assento.setTipoAssento(rs.getString("tipoAssento"));
                assento.setCodVeiculo(rs.getInt("codVeiculo"));
                assento.setTaxaAssento(rs.getInt("taxaAssento"));
                assento.setSituacao(rs.getBoolean("situacao"));
                assentos.add(assento);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOAssento.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return assentos;
    }

}
