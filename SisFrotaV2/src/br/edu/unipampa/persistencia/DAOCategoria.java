/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.persistencia;

import br.edu.unipampa.model.Categoria;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author xxxxxxxxxxxx
 *
 */
public class DAOCategoria {

    public void inserir(Categoria cat) {
        Connection con = ConnectionFactory.conectar();
        String sql = "INSERT INTO categoria(codCat,nomeCat,multiplicador,tipoLinha,situacao) VALUES (?,?,?,?,?)";
        PreparedStatement consulta = null;
        try {
            consulta = con.prepareStatement(sql);
            consulta.setString(1, null);
            consulta.setString(2, cat.getNomeCategoria());
            consulta.setDouble(3, cat.getMultiplicador());
            consulta.setString(4, cat.getTipoLinha());
            consulta.setBoolean(5, cat.isSituacao());
            consulta.execute();
            consulta.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOCategoria.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
    }

    public Categoria buscar(Categoria cat) {
        Connection conexao = ConnectionFactory.conectar();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        Categoria categoria = null;
        String sql = "SELECT * FROM categoria WHERE codCat = ? OR nomeCat=?";
        try {
            consulta = conexao.prepareStatement(sql);
            consulta.setInt(1, cat.getCodCat());
            consulta.setString(2, cat.getNomeCategoria());
            resultado = consulta.executeQuery();
            if (resultado.next()) {
                categoria = new Categoria();
                categoria.setCodCat(resultado.getInt("codCat"));
                categoria.setNomeCategoria(resultado.getString("nomeCat"));
                categoria.setTipoLinha(resultado.getString("tipoLinha"));
                categoria.setMultiplicador(resultado.getDouble("multiplicador"));
            }
        } catch (SQLException e) {
            Logger.getLogger(DAOPagamento.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return categoria;
    }

    public ArrayList<Categoria> buscarAll() {
        Connection con = ConnectionFactory.conectar();
        String sql = "SELECT * FROM categoria WHERE situacao = true";
        PreparedStatement consulta = null;
        ResultSet rs = null;
        Categoria cat = null;
        ArrayList<Categoria> categorias = new ArrayList<>();
        try {
            consulta = con.prepareStatement(sql);
            rs = consulta.executeQuery();
            while (rs.next()) {
                cat = new Categoria();
                cat.setCodCat(rs.getInt("codCat"));
                cat.setNomeCategoria(rs.getString("nomeCat"));
                cat.setMultiplicador(rs.getDouble("multiplicador"));
                cat.setTipoLinha(rs.getString("tipoLinha"));
                cat.setCodPassageiro(rs.getInt("codPass"));
                categorias.add(cat);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOCategoria.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return categorias;
    }

    public ArrayList<Categoria> buscarAllInativos() {
        Connection con = ConnectionFactory.conectar();
        String sql = "SELECT * FROM categoria WHERE situacao = false";
        PreparedStatement consulta = null;
        ResultSet rs = null;
        Categoria cat = null;
        ArrayList<Categoria> categorias = new ArrayList<>();
        try {
            consulta = con.prepareStatement(sql);
            rs = consulta.executeQuery();
            while (rs.next()) {
                cat = new Categoria();
                cat.setCodCat(rs.getInt("codCat"));
                cat.setNomeCategoria(rs.getString("nomeCat"));
                cat.setMultiplicador(rs.getDouble("multiplicador"));
                cat.setTipoLinha(rs.getString("tipoLinha"));
                cat.setCodPassageiro(rs.getInt("codPass"));
                cat.setSituacao(rs.getBoolean("situacao"));
                categorias.add(cat);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOCategoria.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return categorias;
    }

    public void excluir(int id) {
        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("DELETE FROM categoria WHERE codCat = ?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Excluido com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir: " + ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
    }

    public void atualizar(Categoria categoria) {
        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("UPDATE categoria SET nomeCat= ? ,tipoLinha= ?,multiplicador= ?  WHERE codCat = ?");
            stmt.setString(1, categoria.getNomeCategoria());
            stmt.setString(2, categoria.getTipoLinha());
            stmt.setDouble(3, categoria.getMultiplicador());
            stmt.setInt(4, categoria.getCodCat());
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Atualizado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao atualizar: " + ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
    }

    public void atualizarSituacao(Categoria cat) {
        Connection con = ConnectionFactory.conectar();
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("UPDATE categoria SET situacao = false  where codCat = ?");
            stmt.setInt(1, cat.getCodCat());
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Inativado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro : " + ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
    }
}
