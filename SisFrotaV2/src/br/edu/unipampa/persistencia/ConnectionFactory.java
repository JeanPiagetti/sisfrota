/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JPiagetti
 */
public class ConnectionFactory {

    public static Connection conectar() {
        Connection conexao = null;
        try {
            // Registrando a classe JDBC, e os parâmetros de conexão em tempo de execução
            String url = "jdbc:mysql://localhost:3308/sisfrota";
            String usuario = "root";
            String senha = "root";
            conexao = DriverManager.getConnection(url, usuario, senha);
            return conexao;
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conexao;
    }

    public static void fecharConexao() {
        try {
            conectar().close();
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
