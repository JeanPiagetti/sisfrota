/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.persistencia;

import br.edu.unipampa.model.Passageiro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JPiagetti
 */
public class DAOPassageiro {

    public void inserir(Passageiro p) {

        String sql = "INSERT INTO sisfrota.passageiro (`nomePass`, cpf, idade) VALUES(?,?,?)";
        Connection con = ConnectionFactory.conectar();
        PreparedStatement consulta = null;

        try {
            consulta = con.prepareStatement(sql);
            consulta.setString(1, p.getNomePassageiro());
            consulta.setLong(2, p.getCpf());
            consulta.setInt(3, p.getIdade());

        } catch (SQLException ex) {
            Logger.getLogger(DAOPagamento.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.fecharConexao();
        }
    }

    public Passageiro buscar(Passageiro passageiro) {
        Connection conexao = ConnectionFactory.conectar();
        PreparedStatement consulta = null;
        ResultSet resultado = null;
        Passageiro retorno = null;

        String sql = "SELECT * from passageiro where nomePass = ?";

        try {
            consulta = conexao.prepareStatement(sql);
            consulta.setString(1, passageiro.getNomePassageiro());
            resultado = consulta.executeQuery();

            if (resultado.next()) {
                retorno = new Passageiro();
                retorno.setIdade(resultado.getInt("codPass"));
                retorno.setNomePassageiro(resultado.getString("nomePass"));
                retorno.setCpf(resultado.getLong("cpf"));
                retorno.setIdade(resultado.getInt("idade"));
            }
        } catch (SQLException e) {
            Logger.getLogger(DAOPagamento.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            ConnectionFactory.fecharConexao();
        }
        return retorno;
    }

    public ArrayList<Passageiro> buscarAll() {
        return null;
    }

    public void deletar() {

    }
}
