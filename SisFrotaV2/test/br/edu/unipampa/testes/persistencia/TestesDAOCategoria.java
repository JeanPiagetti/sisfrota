/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.testes.persistencia;

import br.edu.unipampa.model.Categoria;
import br.edu.unipampa.model.Passageiro;
import br.edu.unipampa.persistencia.DAOCategoria;
import br.edu.unipampa.persistencia.DAOPassageiro;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author JPiagetti
 */
public class TestesDAOCategoria {

    public TestesDAOCategoria() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Ignore
    @Test
    public void testInsereCategoria() {

        DAOCategoria dao = new DAOCategoria();
        String nomeCategoria = "Idoso";
        String tipoLinha = "Municipal";
        Categoria cat = new Categoria(nomeCategoria, tipoLinha, 2.5);
        dao.inserir(cat);
        assertEquals(dao.buscar(cat).getNomeCategoria(), "Idoso");
    }

    @Ignore
    @Test
    public void testInserePassageiro() {
        Passageiro passageiro = new Passageiro(122, "Joao", 2, 21);
        DAOPassageiro dao = new DAOPassageiro();
        dao.inserir(passageiro);
        assertEquals(dao.buscar(passageiro).getNomePassageiro(), "Joao");

    }
    @Test
    public void testBuscarCategoria(){
       
        DAOCategoria dao = new DAOCategoria();
        String nomeCategoria = "Idoso";
        String tipoLinha = "Municipal";
        Categoria cat = new Categoria(nomeCategoria, tipoLinha, 2.5);
        assertEquals(dao.buscar(cat).getNomeCategoria(),"Idoso");
    }
}
