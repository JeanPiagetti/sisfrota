/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.testes.persistencia;

import br.edu.unipampa.model.Pagamento;
import br.edu.unipampa.persistencia.DAOPagamento;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author JPiagetti
 */
public class TestesDAOPagamento {

    public TestesDAOPagamento() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    @Ignore
    @Test
    public void testDaoPagamentoInserir() {
        DAOPagamento dao = new DAOPagamento();
        Pagamento pagamento = new Pagamento("ButiáELO", "Boleto", "Débito");
        dao.inserir(pagamento);
        assertEquals(dao.buscar(14).getNomeFormaPagamento(), "ButiáELO");
    }

    @Test
    public void testDaoExcluir() {
        DAOPagamento dao = new DAOPagamento();
        Pagamento pagamento = new Pagamento("ButiáELO", "Boleto", "Débito");
        //dao.inserir(pagamento);
        dao.excluir(pagamento.getIdPagamento());
        assertNotEquals(dao.buscar(20).getNomeFormaPagamento(), "ButiáELO");
    }
}
